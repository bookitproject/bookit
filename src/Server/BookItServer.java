package Server;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

import org.apache.http.HttpStatus;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.AmazonS3Exception;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.amazonaws.services.s3.model.S3ObjectSummary;

import Resource.Constants;
import Resource.ImageLibrary;

public class BookItServer extends Thread {
	private ServerSocket ss;
	private SQLDriver sqlDriver;
	
	private Vector<ServerThread> serverThreads;
	private Map<String, ServerThread> usernameThreadMap;
	private AmazonS3Client s3client;
	
	private final static String bucketName = "bookitdb";
		
	public BookItServer(int port) {
		serverThreads = new Vector<ServerThread>();
		usernameThreadMap = new HashMap<String, ServerThread>();
		
		sqlDriver = new SQLDriver(this);
		s3client = new AmazonS3Client(new ProfileCredentialsProvider());
		try {
			sqlDriver.connect();
			ss = new ServerSocket(port);
		} catch(IOException ioe) {
			System.out.println("ioe in server: " + ioe.getMessage());
		} 
		
		this.start();
	}
	
	// Listens for connection (login or signup)
	public void run() {
		try {
			while(true) {
				System.out.println("Waiting for connection... ");
				Socket s = ss.accept();
				System.out.println("...Connection from " + s.getInetAddress());
				ServerThread st = new ServerThread(s, this);
				serverThreads.add(st);
			}
		} catch (IOException ioe) {
			System.out.println("Server ioe: " + ioe.getMessage());
		} finally {
			if (ss != null) {
				try {
					ss.close();
					sqlDriver.stop();
				} catch (IOException e) {
					System.out.println("ioe closing ss & DB: " + e.getMessage());
				}
			}
		}
	}
	
	public void saveUsernameWithThread(ServerThread st, String username) { // each server thread will tell server once its client's username
		usernameThreadMap.put(username, st);
	}
	
	public void removeServeThread(ServerThread st) {	
		serverThreads.remove(st);
		usernameThreadMap.values().remove(st);
	}
	
	public SQLDriver getSQLDriver() {
		return sqlDriver;
	}
	
	public void sendMessageTo(String sendname, String receiverName, String message) {
		// Get serverthread in map by the receiver's username and send the message
		ServerThread sendTo = usernameThreadMap.get(receiverName);
		if (sendTo == null) {
			System.out.println("sendTo - null");
		} else {
			System.out.println("sendTo - not null");
		}
		sendTo.send_message(sendname, receiverName, message);
		System.out.println("Server: forward "+message+" "+sendname+"->"+receiverName);
	}
	
	public Boolean isOnline(String username) {
		System.out.println(username+" remove "+usernameThreadMap.containsKey(username));
		return usernameThreadMap.containsKey(username);
	}
	
	public void uploadImages(Vector<ImageIcon> images, int bookID) {
		if (images != null) {
			for (int i = 0; i < images.size(); i++) {
		        try {
		            System.out.println("Uploading a new object to S3 from a file");
		            File file = new File("image");
		            String keyName = "books/" + bookID + "/image" + i + ".jpg";
		            Image image = images.get(i).getImage();
		            BufferedImage bi = ImageLibrary.getRenderedImage(image);
		            try {
						ImageIO.write(bi, "jpg", file);
					} catch (IOException ioe) {
						System.out.println("ioe in writing image: " + ioe.getMessage());
					}
		            PutObjectRequest por = new PutObjectRequest(bucketName, keyName, file);
		            s3client.putObject(por);
		            try {
						Files.deleteIfExists(file.toPath());
					} catch (IOException ioe) {
						System.out.println("ioe in deleting file: " + ioe.getMessage());
					}
		         } catch(AmazonServiceException ase) {		           
		            System.out.println("ase: " + ase.getMessage());
		        } catch(AmazonClientException ace) {
		            System.out.println("ace: " + ace.getMessage());
		        }
			}
		}
	}
	
	public void uploadProfileImage(ImageIcon icon, String username) {
		if (icon != null) {
			try {
	            System.out.println("Uploading a new object to S3 from a file");
	            File file = new File("image");
	            String keyName = "users/" + username + "/profile.jpg";
	            Image image = icon.getImage();
	            BufferedImage bi = ImageLibrary.getRenderedImage(image);
	            try {
					ImageIO.write(bi, "jpg", file);
				} catch (IOException ioe) {
					System.out.println("ioe in writing profile image: " + ioe.getMessage());
				}
	            s3client.putObject(new PutObjectRequest(bucketName, keyName, file));
	            try {
					Files.deleteIfExists(file.toPath());
				} catch (IOException ioe) {
					System.out.println("ioe in deleting profile file: " + ioe.getMessage());
				}
	         } catch(AmazonServiceException ase) {		           
	            System.out.println("ase: " + ase.getMessage());
	        } catch(AmazonClientException ace) {
	            System.out.println("ace: " + ace.getMessage());
	        }
		}
	}
	
	public Vector<ImageIcon> returnBookImages(int bookID) {
		ListObjectsRequest listObjectsRequest = new ListObjectsRequest().withBucketName(bucketName).withPrefix("books/"+bookID+"/");
		ObjectListing objectListing;
		Vector<ImageIcon> images = new Vector<ImageIcon>();
		do {
			objectListing = s3client.listObjects(listObjectsRequest);
			for (S3ObjectSummary objectSummary : objectListing.getObjectSummaries()) {
				String objectKey = objectSummary.getKey();
				if (objectKey.equals("books/" + bookID + "/")) {
					continue;
				}
				S3Object object = s3client.getObject(new GetObjectRequest(bucketName, objectKey));
				S3ObjectInputStream ois = object.getObjectContent();
				try {
					Image img = ImageIO.read(ois);
					ImageIcon icon = new ImageIcon(img);
					images.addElement(icon);
					ois.close();
				} catch (IOException ioe) {
					System.out.println("ioe in downloadbookimages: " + ioe.getMessage());
				}
			}
			listObjectsRequest.setMarker(objectListing.getNextMarker());
		} while (objectListing.isTruncated());
		
		return images;
	}
	
	public ImageIcon returnProfileImage(String username) {
		String objectKey = "users/" + username + "/profile.jpg";
		S3Object object = null;
		try {
			object = s3client.getObject(new GetObjectRequest(bucketName, objectKey));
		} catch (AmazonS3Exception e) {
			if (e.getStatusCode() == HttpStatus.SC_NOT_FOUND) {
				System.out.println("not found");
				objectKey = "users/anon.jpg";
				object = s3client.getObject(new GetObjectRequest(bucketName, objectKey));
			}
		}
		System.out.println(object.toString());
		S3ObjectInputStream ois = object.getObjectContent();
		Image img = null;
		try {
			img = ImageIO.read(ois);
			ois.close();
		} catch (IOException ioe) {
			System.out.println("ioe in downloadprofileimages1: " + ioe.getMessage());
		}
		ImageIcon icon = new ImageIcon(img);
		return icon;
	}
	
	public static void main(String [] args) {
		new BookItServer(Constants.port);
	}
}
