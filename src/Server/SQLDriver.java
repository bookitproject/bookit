package Server;

import java.security.SecureRandom;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

import Object.BookObject;
import Object.User;
import Resource.Constants;
import Resource.EmailManager;
import Resource.MD5;

public class SQLDriver {
	private final static String url = "jdbc:mysql://bookitdb.cen3xx8axp3w.us-west-2.rds.amazonaws.com:3306/";
	private final static String username = "root";
	private final static String password = "201finalproject";
	private final static String dbname = "bookitdb";

	private final static String newUser = "INSERT INTO Users (username, password, email, wishlist) VALUES (?, ?, ?, ?)";
	private final static String searchBookByTitle = "SELECT * FROM Books WHERE title=?";
	private final static String addBook = "INSERT INTO Books (title, author, edition, price, conditions, courseName, courseNum, seller) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
	private final static String removeBook = "DELETE FROM Books WHERE bookID=?";
	private final static String getUserID = "SELECT * FROM Users WHERE username=?";
	private final static String getBookID = "SELECT * FROM Books WHERE title=? AND seller=?";
	private final static String authEmail = "SELECT * FROM Users WHERE email=?";
	private final static String getSellList = "SELECT * FROM Books WHERE seller=?";
	private final static String getAllWLTables = "SELECT table_name FROM information_schema.tables WHERE table_name LIKE '%wishlist'";

	private BookItServer server;

	private Connection conn;

	public SQLDriver(BookItServer server) {
		try {
			this.server = server;
			Class.forName("com.mysql.jdbc.Driver");
		} catch(ClassNotFoundException cnfe) {
			System.out.println("cnfe in driver: " + cnfe.getMessage());
		}
	}

	public void connect() {
		try {
			conn = DriverManager.getConnection(url + dbname, username, password);
		} catch(SQLException sqle) {
			System.out.println("sqle in connect: " + sqle.getMessage());
		} 
	}

	public void stop() {
		try {
			conn.close();
		} catch(SQLException sqle) {
			System.out.println("sqle in stop driver: " + sqle.getMessage());
		}
	}

	// User-related
	public boolean hasUsername(String username) {
		try {
			PreparedStatement ps = conn.prepareStatement("SELECT * FROM Users WHERE username=?");
			ps.setString(1, username);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				return true;
			}
			rs.close();
			ps.close();
		} catch (SQLException sqle) {
			System.out.println("sqle in hasUsername: " + sqle.getMessage());
		}

		return false;
	}

	public void createNewUser(String username, String password, String email) {
		try {
			PreparedStatement ps = conn.prepareStatement(newUser);
			ps.setString(1, username);
			ps.setString(2, password);
			ps.setString(3, email);
			ps.setString(4, username+"wishlist");
			ps.executeUpdate();
			ps = conn.prepareStatement("CREATE TABLE " + username+"wishlist" + " ("
					+ "bookID INT(11) "
//					+ "FOREIGN KEY (bookID) REFERENCES Books(bookID)"
					+ ")");
			ps.executeUpdate();
			ps.close();
		} catch (SQLException sqle) {
			System.out.println("sqle in createNewUser: " + sqle.getMessage());
		}
	}

	public void updatePassword(String username, String password) {
		try {
			PreparedStatement ps = conn.prepareStatement("UPDATE Users SET password=? WHERE username=?");
			ps.setString(1, password);
			ps.setString(2, username);
			System.out.println("in slq"+ password+" "+username +" " + dbname);
			ps.executeUpdate();
			ps.close();
		} catch(SQLException sqle) {
			System.out.println("sqle in updatePassword: " + sqle.getMessage());
		}
	}

	public void updateEmail(String username, String email) {
		try {
			PreparedStatement ps = conn.prepareStatement("UPDATE Users SET email=? WHERE username=?");
			ps.setString(1, email);
			ps.setString(2, username);
			ps.executeUpdate();
			ps.close();
		} catch(SQLException sqle) {
			System.out.println("sqle: " + sqle.getMessage());
		}
	}

	public Boolean emailExist(String email) {
		try {
			PreparedStatement ps = conn.prepareStatement("SELECT * FROM Users WHERE email=?");
			ps.setString(1, email);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				return true;
			}
			rs.close();
			ps.close();
		} catch (SQLException sqle) {
			System.out.println("sqle in hasEmail: " + sqle.getMessage());
		}

		return false;
	}

	public boolean authenticate(String username, String password) {
		try {
			PreparedStatement ps = conn.prepareStatement("SELECT * FROM Users WHERE username=?");
			ps.setString(1, username);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				if (rs.getString("username").equals(username)) {
					if (rs.getString("password").equals(password)) {
						return true;
					}
				}
			}
			rs.close();
			ps.close();
		} catch (SQLException sqle) {
			System.out.println("sqle in authenticate: " + sqle.getMessage());
		}
		return false;
	}

	// Book-related
	public Vector<BookObject> getBooks(String title) {
		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement(searchBookByTitle);
			ps.setString(1, title);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {

			}
			rs.close();
			ps.close();
		} catch (SQLException sqle) {
			System.out.println("sqle in getBooks: " + sqle.getMessage());
		} 
		return null;
	}

	public BookObject getBook(String title, String username) {
		PreparedStatement ps = null;
		BookObject book = null;
		try {
			ps = conn.prepareStatement("SELECT * FROM Books WHERE title=? AND seller=?");
			ps.setString(1, title);
			ps.setString(2, username);
			ResultSet rs = ps.executeQuery();
			//(title, author, edition, price, conditions, courseName, courseNum, seller)
			while(rs.next()) {
				String t = rs.getString("title");
				String author = rs.getString("author");
				int edition = rs.getInt("edition");
				float price = rs.getFloat("price");
				String cond = rs.getString("conditions");
				String courseType = rs.getString("courseName");
				int courseNumber = rs.getInt("courseNum");
				String name = rs.getString("seller");
				book = new BookObject(t,author,edition,price,cond,courseType,courseNumber,name);
			}
			rs.close();
			ps.close();
		} catch (SQLException sqle) {
			System.out.println("sqle in getBooks: " + sqle.getMessage());
		} 
		if (book == null) System.out.println("Driver: getBook for "+title+" doesn't exist!");
		return book;
	}

	public Boolean addBook(BookObject book) {
		Boolean existsAlready = false;
		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement("SELECT * FROM Books WHERE title=? AND seller=?");
			ps.setString(1, book.getTitle());
			ps.setString(2, book.getSellerUsername());
			ps.executeQuery();
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				existsAlready = true;
			} else {
				System.out.println("Driver: adding new book");
				ps = conn.prepareStatement(addBook);
				ps.setString(1, book.getTitle());
				ps.setString(2, book.getAuthor());
				ps.setInt(3, book.getEdition());
				ps.setFloat(4, book.getPrice());
				int condition = getConditionNumber(book.getCondition());
				ps.setInt(5, condition);
				ps.setString(6, book.getCourseType());
				ps.setInt(7, book.getCourseNumber());
				ps.setString(8, book.getSellerUsername());
				ps.executeUpdate();
				ps = conn.prepareStatement("SELECT bookID FROM Books ORDER BY bookID DESC LIMIT 1");
				rs = ps.executeQuery();
				int bookID = 0;
				while (rs.next()) {
					bookID = rs.getInt("bookID");
				}
				System.out.println("Driver addBook: added a book (" + bookID + ")");
				server.uploadImages(book.getImages(), bookID);
			}
			rs.close();
			ps.close();
		} catch (SQLException sqle) {
			System.out.println("sqle in addBooks: " + sqle.getMessage());
		}
		return existsAlready; 
	}

	public void deleteBook(BookObject book) {
		
		PreparedStatement ps = null;
		int bookID = getBookID(book.getTitle(), book.getSellerUsername());
		System.out.println("Driver delete: " + book.getTitle() + "(" + bookID+") from " + book.getSellerUsername());
		
		try {
			ps = conn.prepareStatement(removeBook);
			ps.setInt(1, bookID);
			ps.executeUpdate();
			// delete book from all wish lists
			deleteBookFromAllWishLists(bookID);
			ps.close();
		} catch (SQLException sqle) {
			System.out.println("sqle in deleteBooks: " + sqle.getMessage());
		} 
	}

	public int getUserID(String username) {
		int userID = 0;
		try {
			PreparedStatement ps = conn.prepareStatement(getUserID);
			ps.setString(1, username);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				userID = rs.getInt("userID");
			}
		} catch (SQLException sqle) {
			System.out.println("sqle in getUserID: " + sqle.getMessage());
		}
		return userID;
	}

	public int getBookID(String title, String seller) {
		int bookID = 0;
		try {
			PreparedStatement ps = conn.prepareStatement(getBookID);
			ps.setString(1, title);
			ps.setString(2, seller);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				bookID = rs.getInt("bookID");
			}
		} catch (SQLException sqle) {
			System.out.println("sqle in getBookID: " + sqle.getMessage());
		}
		return bookID;
	}

	// Sends message to an offline user
	public void sendOfflineMessage(String toUser, String message, String fromUser) {
		// Get email for toUser and fromUser
		String toEmail = "";
		String fromEmail = "";
		try {
			PreparedStatement ps = conn.prepareStatement("SELECT * FROM Users WHERE username=?");
			ps.setString(1, toUser);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				toEmail = rs.getString("email");
			}
			
			ps = conn.prepareStatement("SELECT * FROM Users WHERE username=?");
			ps.setString(1, fromUser);
			rs = ps.executeQuery();
			while (rs.next()) {
				fromEmail = rs.getString("email");
			}
			ps.close();
			rs.close();
			
			System.out.println("Driver: sent email " + fromUser + "->" + toUser);
		} catch (SQLException sqle) {
			System.out.println("sqle in sendOfflineEmail: " + sqle.getMessage());
		}
		
		new EmailManager().sendTextEmail(toEmail, fromEmail, fromUser, message);
	}
	
	// Checks that the email provided is valid and sends an email with username & password
	public boolean authenticateEmailAndSendEmail(String email) {
		try {
			PreparedStatement ps = conn.prepareStatement(authEmail);
			ps.setString(1, email);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				String username = rs.getString("username");
				String password = randomString(8);
				String hash = MD5.encrypt(password);
				updatePassword(username, hash);
				new EmailManager().sendForgotPasswordEmail(email, username, password);
				return true;
			}
		} catch (SQLException sqle) {
			System.out.println("sqle in authEmail: " + sqle.getMessage());
		}
		return false;
	}

	// For generating temporary password
	private String randomString( int len ){
		String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
		SecureRandom rnd = new SecureRandom();
		
		StringBuilder sb = new StringBuilder( len );
		for( int i = 0; i < len; i++ ) 
			sb.append( AB.charAt( rnd.nextInt(AB.length()) ) );
		return sb.toString();
	}

	public Vector<BookObject> returnSearchResults(String keyWord, int criteria) {
		Vector<BookObject> searchObjects = new Vector<BookObject>();
		PreparedStatement ps = null;
		try {
			if (criteria == 1) {
				ps = conn.prepareStatement("SELECT * FROM Books WHERE title LIKE " + "'%" + keyWord + "%'");
			} else if (criteria == 2) {
				String [] parts = keyWord.split(" ");
				ps = conn.prepareStatement("SELECT * FROM Books WHERE courseName=? AND courseNum=?");
				ps.setString(1, parts[0]);
				ps.setInt(2, Integer.parseInt(parts[1]));
			} else {
				ps = conn.prepareStatement("SELECT * FROM Books WHERE author LIKE " + "'%" + keyWord + "%'");
			}

			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				String title = rs.getString("title");
				String author = rs.getString("author");
				int edition = rs.getInt("edition");
				float price = rs.getFloat("price");
				int cond = rs.getInt("conditions");
				String condition = getConditionName(cond);
				String courseType = rs.getString("courseName");
				int courseNumber = rs.getInt("courseNum");
				String username = rs.getString("seller");
				BookObject bo = new BookObject(title, author, edition, price, condition, courseType, courseNumber, username);
				searchObjects.add(bo);
			}

			rs.close();
			ps.close();
		} catch(SQLException sqle) {
			System.out.println("sqle in search: " + sqle.getMessage());
		}

		return searchObjects;
	}

	public Vector<BookObject> returnSortedSearch(String keyWord, int criteria, int sortCriteria) {
		Vector<BookObject> sortObjects = new Vector<BookObject>();
		PreparedStatement ps = null;
		try {
			if (criteria == 1) {
				if (sortCriteria == 1) {
					ps = conn.prepareStatement("SELECT * FROM Books WHERE title LIKE " + "'%" + keyWord + "%' ORDER BY title ASC");
				} else if (sortCriteria == 2) {
					ps = conn.prepareStatement("SELECT * FROM Books WHERE title LIKE " + "'%" + keyWord + "%' ORDER BY courseName ASC, courseNum ASC");
				} else if (sortCriteria == 3) {
					ps = conn.prepareStatement("SELECT * FROM Books WHERE title LIKE " + "'%" + keyWord + "%' ORDER BY price ASC");
				} else {
					ps = conn.prepareStatement("SELECT * FROM Books WHERE title LIKE " + "'%" + keyWord + "%' ORDER BY conditions ASC");
				}
			} else if (criteria == 2) {
				String [] parts = keyWord.split("\\s+");
				if (sortCriteria == 1) {
					ps = conn.prepareStatement("SELECT * FROM Books WHERE courseName=? AND courseNum=? ORDER BY title ASC");
					ps.setString(1, parts[0]);
					ps.setInt(2, Integer.parseInt(parts[1]));
				} else if (sortCriteria == 2) {
					ps = conn.prepareStatement("SELECT * FROM Books WHERE courseName=? AND courseNum=? ORDER BY courseName ASC, courseNum ASC");
					ps.setString(1, parts[0]);
					ps.setInt(2, Integer.parseInt(parts[1]));
				} else if (sortCriteria == 3) {
					ps = conn.prepareStatement("SELECT * FROM Books WHERE courseName=? AND courseNum=? ORDER BY price ASC");
					ps.setString(1, parts[0]);
					ps.setInt(2, Integer.parseInt(parts[1]));
				} else {
					ps = conn.prepareStatement("SELECT * FROM Books WHERE courseName=? AND courseNum=? ORDER BY conditions ASC");
					ps.setString(1, parts[0]);
					ps.setInt(2, Integer.parseInt(parts[1]));
				}
			} else {
				if (sortCriteria == 1) {
					ps = conn.prepareStatement("SELECT * FROM Books WHERE author LIKE " + "'%" + keyWord + "%' ORDER BY title ASC");
				} else if (sortCriteria == 2) {
					ps = conn.prepareStatement("SELECT * FROM Books WHERE author LIKE " + "'%" + keyWord + "%' ORDER BY courseName ASC, courseNum ASC");
				} else if (sortCriteria == 3) {
					ps = conn.prepareStatement("SELECT * FROM Books WHERE author LIKE " + "'%" + keyWord + "%' ORDER BY price ASC");
				} else {
					ps = conn.prepareStatement("SELECT * FROM Books WHERE author LIKE " + "'%" + keyWord + "%' ORDER BY conditions ASC");
				}
			}

			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				String title = rs.getString("title");
				String author = rs.getString("author");
				int edition = rs.getInt("edition");
				float price = rs.getFloat("price");
				int cond = rs.getInt("conditions");
				String condition = getConditionName(cond);
				String courseType = rs.getString("courseName");
				int courseNumber = rs.getInt("courseNum");
				String username = rs.getString("seller");
				BookObject bo = new BookObject(title, author, edition, price, condition, courseType, courseNumber, username);
				sortObjects.add(bo);
			}

			rs.close();
			ps.close();
		} catch(SQLException sqle) {
			System.out.println("sqle in sort: " + sqle.getMessage());
		}

		return sortObjects;
	}

	public User returnUserSearch(String username) {
		User user = null;
		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement("SELECT * FROM Users WHERE username=?");
			ps.setString(1, username);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				String name = rs.getString("username");
				String pw = rs.getString("password");
				user = new User(name, pw);
			}
			rs.close();
			ps.close();
		} catch(SQLException sqle) {
			System.out.println("sqle in search: " + sqle.getMessage());
		}
		return user;
	}

	public int getConditionNumber(String cond) {
		if (cond.equals(Constants.conditionExcellent)) {
			return 1;
		}
		else if (cond.equals(Constants.conditionGood)) {
			return 2;
		}
		else if (cond.equals(Constants.conditionOkay)) {
			return 3;
		}
		return 4;
	}

	public String getConditionName(int cond) {
		if (cond == 1) {
			return Constants.conditionExcellent;
		}
		else if (cond == 2) {
			return Constants.conditionGood;
		}
		else if (cond == 3) {
			return Constants.conditionOkay;
		}
		return Constants.conditionPoor;
	}

	public Vector<BookObject> returnSellList(String username) {
		Vector<BookObject> selling = new Vector<BookObject>();
		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement(getSellList);
			ps.setString(1, username);
			ResultSet rs = ps.executeQuery();
			//			if (!rs.next()) {
			//				selling = null;
			//				return null;
			//			}
			while (rs.next()) {
				String title = rs.getString("title");
				String author = rs.getString("author");
				int edition = rs.getInt("edition");
				float price = rs.getFloat("price");
				int cond = rs.getInt("conditions");
				String condition = getConditionName(cond);
				String courseType = rs.getString("courseName");
				int courseNumber = rs.getInt("courseNum");
				BookObject bo = new BookObject(title, author, edition, price, condition, courseType, courseNumber, username);
				selling.add(bo);
			}
			rs.close();
			ps.close();
		} catch(SQLException sqle) {
			System.out.println("sqle in returnSellList: " + sqle.getMessage());
		}
		for (BookObject b:selling) {
			System.out.println("Driver Sell: " + b.getTitle());
		}
		return selling;
	}

	public Vector<BookObject> returnWishList(String username) {
		Vector<BookObject> wishlist = new Vector<BookObject>();
		System.out.println("sqle d:" + username);
		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement("SELECT * FROM " + username +"wishlist");
			ResultSet rs = ps.executeQuery();
			//			if (!rs.next()) {
			//				System.out.println("No books to return");
			////				wishlist = null;
			//				return null;
			//			} 
			while (rs.next()) {
				int bookID = rs.getInt("bookID");
				System.out.println("sqle d:" + bookID);
				PreparedStatement ps2 = conn.prepareStatement("SELECT * FROM Books WHERE bookID=?");
				ps2.setInt(1, bookID);
				ResultSet rs2 = ps2.executeQuery();
				while (rs2.next()) {
					String title = rs2.getString("title");
					String author = rs2.getString("author");
					int edition = rs2.getInt("edition");
					float price = rs2.getFloat("price");
					int cond = rs2.getInt("conditions");
					String condition = getConditionName(cond);
					String courseType = rs2.getString("courseName");
					int courseNumber = rs2.getInt("courseNum");
					String seller = rs2.getString("seller");
					BookObject bo = new BookObject(title, author, edition, price, condition, courseType, courseNumber, seller);
					wishlist.add(bo);
				}
				rs2.close();
				ps2.close();
			}
			rs.close();
			ps.close();
		} catch(SQLException sqle) {
			System.out.println("sqle in returnWL: " + sqle.getMessage());
		}

		return wishlist;
	}

	public void addToWishList(String username, BookObject book) {
		int bookID = getBookID(book.getTitle(), book.getSellerUsername());
		PreparedStatement ps = null;
		try {
			// If book isn't already in wishlist, add it
			PreparedStatement ps2 = conn.prepareStatement("SELECT * FROM " + username + "wishlist WHERE bookID=?");
			ps2.setInt(1, bookID);
			ResultSet rs2 = ps2.executeQuery();
			if (!rs2.next()) {
				ps = conn.prepareStatement("INSERT INTO " + username + "wishlist (bookID) VALUES (?)");
				ps.setInt(1, bookID);
				ps.executeUpdate();
				ps.close();
			}
		} catch(SQLException sqle) {
			System.out.println("sqle in addWL: " + sqle.getMessage());
		}
		System.out.println("SQLDriver: "+book.getSellerUsername() + "'s "+book.getTitle()+"("+bookID+") to "+ username + "'s WL");
	}

	// Remove book sold by seller from username's WL
	public void deleteFromWishList(BookObject book, String username) {
		int bookID = getBookID(book.getTitle(), book.getSellerUsername());
		System.out.println("Driver deleteWL: " + book.getTitle() + "(" + bookID + ") from " + username);
		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement("DELETE FROM " + username + "wishlist WHERE bookID=?");
			ps.setInt(1, bookID);
			ps.executeUpdate();
			ps.close();
		} catch(SQLException sqle) {
			System.out.println("sqle in deleteWL: " + sqle.getMessage());
		}
	}
	
	public void deleteBookFromAllWishLists(int bookID) {
		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement(getAllWLTables);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				String table = rs.getString("table_name");
				System.out.println("DeleteWL from: " + table);
				ps = conn.prepareStatement("DELETE FROM " + table + " WHERE bookID=?");
				ps.setInt(1, bookID);
				ps.executeUpdate();
			}
			rs.close();
			ps.close();
		} catch (SQLException sqle) {
			System.out.println("sqle in deleteAllFromWL: " + sqle.getMessage());
		}
	}
}
