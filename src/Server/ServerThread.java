package Server;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Vector;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import javax.swing.ImageIcon;

import Object.BookObject;
import Object.ChatObject;
import Object.MessageToClient;
import Object.MessageToServer;
import Object.User;
import Resource.Constants;

public class ServerThread extends Thread {
	
	private ObjectInputStream ois;
	private ObjectOutputStream oos;
	private BookItServer server;
	private Socket s;
	
	public ServerThread(Socket s, BookItServer server) {
		this.s = s;
		this.server = server;
		try {
			oos = new ObjectOutputStream(s.getOutputStream());
			ois = new ObjectInputStream(s.getInputStream());
			this.start(); 
		} catch (IOException e) {
			System.out.println("ioe st: " + e.getMessage());
		}
	}
	
	public void run() {
		// Listens to client for book things (get, add, remove), login and signup (authenticate, add/remove user), and chat messages
		try {
			while(true) {
				MessageToServer msg = (MessageToServer)ois.readObject();
				String description = msg.getDescription();
				System.out.println("> ST received: " + description);
				
				if (description.equals(Constants.getBooks)) {
					Vector<BookObject> bookResults = server.getSQLDriver().returnSortedSearch(msg.getSearchWord(), msg.getCriteria(), msg.getSortCriteria());
					oos.writeObject(new MessageToClient(description, null, null, bookResults, null));
					oos.flush();
					System.out.println("ST sends: " + description);
				} else if (description.equals(Constants.getBook)) {
					// TODO: retrieve book & flush to client -- do setbook from msg.getBook()
					String title = msg.getBook().getTitle();
					String seller = msg.getBook().getSellerUsername();
					
				} else if (description.equals(Constants.getUser)) {
					User searchedUser = server.getSQLDriver().returnUserSearch(msg.getSearchWord());
					oos.writeObject(new MessageToClient(description, null, null, null, searchedUser));
					oos.flush();
					System.out.println("ST sends: " + description);
					
					
					
				} else if (description.equals(Constants.addBook)) {
					Boolean existsAlready = server.getSQLDriver().addBook(msg.getBook());
					Vector<BookObject> sellBooks = server.getSQLDriver().returnSellList(msg.getBook().getSellerUsername());
					System.out.println("ST addBook: returning " + sellBooks.size() + " sell books");
					for (BookObject b : sellBooks) {
//						System.out.println("ST sellB: " + b.getTitle());
					}
					oos.writeObject(new MessageToClient(description, existsAlready, null, sellBooks, null));
					oos.flush();
				} else if (description.equals(Constants.addToWL)) {
					server.getSQLDriver().addToWishList(msg.getUsername(), msg.getBook());
					Vector<BookObject> wlBooks = server.getSQLDriver().returnWishList(msg.getUsername());
					if (wlBooks == null) System.out.println("ST: "+msg.getUsername() + " has no WL");
					else {
						System.out.println("ST: " + msg.getUsername() + " has " + wlBooks.size() + " WLBooks");
						for (BookObject b : wlBooks) {
							System.out.println("  " + b.getSellerUsername() + "'s " + b.getTitle());
						}
					}
					
					oos.writeObject(new MessageToClient(description, null, null, wlBooks, null));
					oos.flush();
					for (BookObject b : wlBooks) {
						System.out.println("  " + b.getTitle() + " " + b.getSellerUsername());
					}
				} else if (description.equals(Constants.deleteBook)) {
					server.getSQLDriver().deleteBook(msg.getBook());
					Vector<BookObject> sBooks = server.getSQLDriver().returnSellList(msg.getBook().getSellerUsername());
					MessageToClient send = new MessageToClient(description, null, null, sBooks, null);
					oos.writeObject(send);
					oos.flush();
					System.out.println("ST deleteBook, now " + sBooks.size() + " books");
				} else if (description.equals(Constants.deleteBookWL)) {
					Vector<BookObject> wlBooks1 = server.getSQLDriver().returnWishList(msg.getUsername());
					int beforeSize = wlBooks1.size();
					server.getSQLDriver().deleteFromWishList(msg.getBook(), msg.getUsername());
					Vector<BookObject> wlBooks = server.getSQLDriver().returnWishList(msg.getUsername());
					oos.writeObject(new MessageToClient(description, null, null, wlBooks, null));
					oos.flush();
					System.out.println("ST deleteWL, before:" + beforeSize + " now:" + wlBooks.size());
				} else if (description.equals(Constants.getSellBooksPriv)) {
					Vector<BookObject> sellBooks = server.getSQLDriver().returnSellList(msg.getUsername());
					oos.writeObject(new MessageToClient(description, null, null, sellBooks, null));
					oos.flush();
					if (sellBooks != null) System.out.println("ST sends: " + description + " " + sellBooks.size() + " books");
				} else if (description.equals(Constants.getSellBooksPub)) {
					Vector<BookObject> sellBooks = server.getSQLDriver().returnSellList(msg.getUsername());
					oos.writeObject(new MessageToClient(description, null, null, sellBooks, null));
					oos.flush();
					if (sellBooks != null) System.out.println("ST sends: " + description + " " + sellBooks.size() + " books");
				} else if (description.equals(Constants.getWLBooks)) {
					Vector<BookObject> wlBooks = server.getSQLDriver().returnWishList(msg.getUsername());
					oos.writeObject(new MessageToClient(description, null, null, wlBooks, null));
					oos.flush();
					if (wlBooks != null) {
						System.out.println("ST sends: " + description + " " + wlBooks.size() + " books");
						for (BookObject b : wlBooks) {
							System.out.println("  " + b.getTitle() + " " + b.getSellerUsername());
						}
					}
				} 
					
					
					
				else if (description.equals(Constants.hasUsername)) {
					Boolean has = server.getSQLDriver().hasUsername(msg.getUsername());
					oos.writeObject(new MessageToClient(description, has, null,null,null));
					oos.flush();
					System.out.println("ST sends: " + description + " " + has);
				} else if (description.equals(Constants.addUser)) {
					// Associate username with this thread and create a new user in DB
					server.saveUsernameWithThread(this, msg.getUser().getUsername());
					server.getSQLDriver().createNewUser(msg.getUser().getUsername(), msg.getUser().getPassword(), msg.getEmail());
				} else if (description.equals(Constants.signin)) {
					// Check if user exists; then return whether credentials are correct
					Boolean userExists = server.getSQLDriver().hasUsername(msg.getUsername());
					System.out.println("ST signin: " + msg.getUsername() + " exists: " + userExists);
					if (!userExists) {
						oos.writeObject(new MessageToClient(description, false, null,null,new User(msg.getUsername(),"")));
						oos.flush();
					} else {
						Boolean canLogin = server.getSQLDriver().authenticate(msg.getUsername(), msg.getPassword());
						System.out.println("ST canLogin: " + canLogin);if (canLogin) {
							server.saveUsernameWithThread(this, msg.getUsername());
						}
						oos.writeObject(new MessageToClient(description, canLogin, null,null,new User(msg.getUsername(),"")));
						oos.flush();
					}
					
				} else if(description.equals(Constants.logout)) {
					server.removeServeThread(this);
				} else if (description.equals(Constants.updatePw)) {
					server.getSQLDriver().updatePassword(msg.getUsername(), msg.getPassword());
				} else if (description.equals(Constants.updateEmail)) {
					Boolean existsAlready = server.getSQLDriver().emailExist(msg.getEmail());
					if (!existsAlready) {
						server.getSQLDriver().updateEmail(msg.getUsername(), msg.getEmail());
					} 
					oos.writeObject(new MessageToClient(description, existsAlready, null, null,null));
					oos.flush();
					
				} else if (description.equals(Constants.checkEmail)) {
					Boolean existsAlready = server.getSQLDriver().emailExist(msg.getEmail());
					oos.writeObject(new MessageToClient(description, existsAlready, null, null,null));
					oos.flush();
				} else if (description.equals(Constants.sendChat)) {
					System.out.println("ServerThread: send to server "+msg.getChat().getMessage() +" "+ msg.getChat().getReceiverName()+" from " +msg.getChat().getSenderName());
					server.sendMessageTo(msg.getChat().getSenderName(), msg.getChat().getReceiverName(), msg.getChat().getMessage());
				} else if (description.equals(Constants.forgotUsernamePw)) {
					Boolean emailed = server.getSQLDriver().authenticateEmailAndSendEmail(msg.getEmail());
					oos.writeObject(new MessageToClient(description, emailed, null, null,null));
					oos.flush();
				} else if (description.equals(Constants.checkOnline)) {
					Boolean isOnline = server.isOnline(msg.getUsername());
					oos.writeObject(new MessageToClient(description, isOnline, null, null, new User(msg.getUsername(),"")));
					oos.flush();
					System.out.println("ST sends: " + description);
				} else if (description.equals(Constants.Onlinechat)) {
					Boolean isOnline = server.isOnline(msg.getUsername());
					oos.writeObject(new MessageToClient(description, isOnline, null, null, new User(msg.getUsername(),"")));
					oos.flush();
					System.out.println("ST sends: " + description);
				} else if (description.equals(Constants.offlineMessage)) {
					String toUser = msg.getUsername();
					String message = msg.getSearchWord();
					String fromUser = msg.getUser().getUsername();
					server.getSQLDriver().sendOfflineMessage(toUser, message, fromUser);
				} else if (description.equals(Constants.getImages)) {
					int bookID = server.getSQLDriver().getBookID(msg.getBook().getTitle(), msg.getBook().getSellerUsername());
					Vector<ImageIcon> images = server.returnBookImages(bookID);
					msg.getBook().setImages(images);
					MessageToClient mtc = new MessageToClient(description, null, null, null, null);
					mtc.setBook(msg.getBook());
					oos.writeObject(mtc);
					oos.flush();
				} else if (description.equals(Constants.getProfilePic)) {
					ImageIcon icon = server.returnProfileImage(msg.getUsername());
					if (icon == null) {
						System.out.println("ST icon is null");
					} else System.out.println("ST icon is not null");
					MessageToClient mtc = new MessageToClient(description, null, null, null, null);
					mtc.setProfilePic(icon);
					oos.writeObject(mtc);
					oos.flush();
					System.out.println("ST finished writing obj");
				} else if (description.equals(Constants.getProfilePicSearch)) {
					ImageIcon icon = server.returnProfileImage(msg.getUsername());
					if (icon == null) {
						System.out.println("ST icon is null");
					} else System.out.println("ST icon is not null");
					MessageToClient mtc = new MessageToClient(description, null, null, null, null);
					mtc.setProfilePic(icon);
					oos.writeObject(mtc);
					oos.flush();
					System.out.println("ST finished writing obj");
				} else if (description.equals(Constants.uploadProfilePic)) {
					server.uploadProfileImage(msg.getIcon(), msg.getUsername());
					MessageToClient mtc = new MessageToClient(description, null, null, null, null);
					mtc.setProfilePic(msg.getIcon());
					oos.writeObject(mtc);
					oos.flush();
				} else if (description.equals(Constants.getSellerAndProfilePic)) {
					ImageIcon icon = server.returnProfileImage(msg.getUsername());
					User user = server.getSQLDriver().returnUserSearch(msg.getUsername());
					if (icon == null) {
						System.out.println("ST icon is null");
					} else System.out.println("ST icon is not null");
					MessageToClient mtc = new MessageToClient(description, null, null, null, user);
					mtc.setProfilePic(icon);
					oos.writeObject(mtc);
					oos.flush();
					System.out.println("ST finished writing obj");
				} 
			}
		} catch (ClassNotFoundException e) {
			System.out.println("ServerThread cnfe: " + e.getMessage());
		} catch (IOException e) {
			System.out.println("ServerThread ioe: " + e.getMessage());
		} finally {
			// Here, something's wrong with the connection with client, so close socket & remove itself from server
			System.out.println("ST: finally");
			if (s != null) {
				try {
					s.close();
				} catch (IOException e) {
					System.out.println("ioe user: " + e.getMessage());
				}
			}
			server.removeServeThread(this);
		}
		
	}
	
	public void send_message(String sender, String receiver, String message) {
		ChatObject chat = new ChatObject(sender, receiver, message);
		MessageToClient msg = new MessageToClient(Constants.sendChat, null, chat, null, null);
		try {
			oos.writeObject(msg);
			oos.flush();
		} catch (IOException e) {
			System.out.println("ServerThread ioe: " + e.getMessage());
		} 
	}

}
