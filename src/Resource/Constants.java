package Resource;

public class Constants {
	
	// For all frames
	public static final int frameHeight = 728;
	public static final int frameWidth = 1024;

	public static final String frameBackgroundFilename = "";
	

	//public static final String hostname = "192.168.23.1";
	public static final String hostname = "localhost";
	public static final int port = 5678;
	
	// For buttons
	public static final String buttonBackgroundFilename = "";
	
	// For book conditions
	public static final String conditionExcellent = "excellent";
	public static final String conditionGood = "good";
	public static final String conditionOkay = "okay";
	public static final String conditionPoor = "poor";
	
	//// For SendMessage descriptions
	// for books
	public static final String getBooks = "getBooks"; // for book search, given search word
	public static final String getBook = "getBook"; // for getting one book
	public static final String getUser = "getUser"; // for user search, given username
	public static final String getSellBooksPriv = "getSellBooksPriv";
	public static final String getSellBooksPub = "getSellBooksPub";
	public static final String getWLBooks = "getWLBooks";
	public static final String addBook = "addBook";
	public static final String addToWL = "addToWL";
	public static final String deleteBook = "deleteBook";
	public static final String deleteBookWL = "deleteBookWL";
	// for users
	public static final String addUser = "addUser";
	public static final String signin = "signin";
	public static final String hasUsername = "hasUsername";
	public static final String updatePw = "updatePw";
	public static final String updateEmail = "updateEmail";
	public static final String checkEmail = "checkEmail";
	public static final String forgotUsernamePw = "forgot";
	public static final String checkOnline = "checkOnline";
	public static final String Onlinechat = "Onlinechat";
	public static final String logout = "logout";
	public static final String offlineMessage = "offlineMessage";
	// for chatting
	public static final String sendChat = "sendChat";
	
	// AddBookGUI
	public static final String getUserID = "getUserID";
	
	// GetImages
	public static final String getImages = "getImages";
	public static final String getProfilePic = "getProfilePic";
	public static final String getProfilePicSearch = "getProfilePicSearch";
	public static final String uploadProfilePic = "uploadProfilePic";
	public static String getSellerAndProfilePic = "gettheuserandprofilepic";
	
}
