package Resource;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.net.URL;

import javax.imageio.ImageIO;

//import from CSCI201Factory
//A statically used class for loading image resources
//Ensures that images aren't loaded into memory more than necessary
public class ImageLibrary {
	private static Map<String, Image> imageMap;
	
	static{
		imageMap = new HashMap<String,Image>();
	}
	
	private ImageLibrary(){} //disable constructor
	
	//Gets the image if available already, otherwise the image is loaded and returned
	public static Image getImage(String directory) {
		/*Image img = imageMap.get(directory);
		if(img == null) {
			try { img = ImageIO.read(new File(directory)); }
			catch (IOException e) { System.out.println("Error reading image: " + e); return null; }
			imageMap.put(directory, img);
		}
		return img;*/
		
		Image img = imageMap.get(directory);
		if(img == null) {
			try {
				if(directory.startsWith("http")) {
					img = ImageIO.read(new URL(directory));
				}
				else img = ImageIO.read(new File(directory));
			} catch(IOException e) {
				System.out.println("Error reading image: "+e);
				return null;
			}
			imageMap.put(directory, img);			
		}
		return img;
	}
	
	//Forced the image to be reloaded from file
	public static Image getImageReload(String directory) {
		Image img;
		try { img = ImageIO.read(new File(directory)); }
		catch (IOException e) { System.out.println("Error reading image: " + e); return null; }
		imageMap.put(directory, img);
		return img;
	}
	
	//Clears out all the images from the library
	public static void clearImages() {
		imageMap.clear();
	}
	
	public static BufferedImage getRenderedImage(Image im) {
		int w = im.getWidth(null);
        int h = im.getHeight(null);
        int type = BufferedImage.TYPE_INT_RGB;
        BufferedImage out = new BufferedImage(w, h, type);
        Graphics2D g2 = out.createGraphics();
        g2.drawImage(im, 0, 0, null);
        g2.dispose();
        return out;
	}
}
