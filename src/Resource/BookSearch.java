package Resource;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Vector;
import Object.BookObject;
public class BookSearch {
	
	//evretmmj
		private String book_name;
		private ArrayList<BookObject> all_book_name;
		private int count;
		
		BookSearch(String book_name,ArrayList<BookObject> all_book_name){
			this.book_name=book_name;
			this.all_book_name=all_book_name;
		}
		
		protected String[] getWord(String book_name){
			String[] word_book=book_name.split(" ");
			return word_book;	
		}
		
		
		protected int count_difference(String[] book_to_search){
			count=0;
			String[] book_name_apart=getWord(book_name);
			int length=book_to_search.length;
			int length2=book_name_apart.length;
			count=Math.abs(length-length2);
			return count;
		}
		
		
		
		public ArrayList<BookObject> get_book_order(){
			ArrayList<BookObject> book_order=new ArrayList<BookObject>();
			for(int i=0;i<all_book_name.size();i++){
				String[] book_search_apart=getWord(all_book_name.get(i).getTitle());
				int difference=count_difference(book_search_apart);
				if(book_order.isEmpty()){
					book_order.add(all_book_name.get(i));
				}else{
					for(int j=0;j<book_order.size();j++){
						if(difference>count_difference(getWord(book_order.get(j).getTitle()))){
							book_order.add(j+1,all_book_name.get(i));
						}
					}
				}
			}
			
			return book_order;
		}
		
		public ArrayList<BookObject> get_book_order_price(){
			ArrayList<BookObject> book_order_price=new ArrayList<BookObject>();
			for(int i=0;i<all_book_name.size();i++){
				float price =all_book_name.get(i).getPrice();
				if(book_order_price.isEmpty()){
					book_order_price.add(all_book_name.get(i));
				}else{
					for(int j=0;j<book_order_price.size();j++){
						if(price>book_order_price.get(j).getPrice()){
							book_order_price.add(j+1,all_book_name.get(i));
						}
						else if(j==book_order_price.size()-1){
							book_order_price.add(j+1,all_book_name.get(i));
						}
					}
				}
			}
			
			return book_order_price;
		}
		
		public ArrayList<BookObject> get_book_order_condition(){
			ArrayList<BookObject> book_order_condition=new ArrayList<BookObject>();
			for(int i=0;i<all_book_name.size();i++){
				String condition =all_book_name.get(i).getCondition();
				if(book_order_condition.isEmpty()){
					book_order_condition.add(all_book_name.get(i));
				}else{
					for(int j=0;j<book_order_condition.size();j++){
						if(condition.charAt(0)>book_order_condition.get(j).getCondition().charAt(0)){
							book_order_condition.add(j+1,all_book_name.get(i));
						}
						else if(j==book_order_condition.size()-1){
							book_order_condition.add(j+1,all_book_name.get(i));
						}
					}
				}
			}
			
			return book_order_condition;
		}
		
		public ArrayList<BookObject> get_book_order_class(){
			ArrayList<BookObject> book_order_class=new ArrayList<BookObject>();
			for(int i=0;i<all_book_name.size();i++){
				String class_type =all_book_name.get(i).getCourseType();
				int class_number =all_book_name.get(i).getCourseNumber();
				if(book_order_class.isEmpty()){
					book_order_class.add(all_book_name.get(i));
				}else{
					for(int j=0;j<book_order_class.size();j++){
						if(class_type.equals(book_order_class.get(j).getCourseType())){
							if(class_number>book_order_class.get(j).getCourseNumber())
							{
								book_order_class.add(j+1,all_book_name.get(i));
							}
							else if(j==book_order_class.size()-1)
							{
								book_order_class.add(j+1,all_book_name.get(i));
							}
						}
						else{
							if(!compare(class_type,book_order_class.get(j).getCourseType())){
								book_order_class.add(j+1,all_book_name.get(i));
							}
							else if(j==book_order_class.size()-1)
							{
								book_order_class.add(j+1,all_book_name.get(i));
							}
						}
					}
				}
			}
			
			return book_order_class;
		}
		//getCouseType getCourceNumber
		public boolean compare(String name1,String name2){
			boolean check=true;
			int minimum=Math.min(name1.length(),name2.length());
			if(minimum==name1.length()){
				boolean checkequal=true;
				for(int i=0;i<minimum;i++){
					if(name1.charAt(i)!=name2.charAt(i)){
						checkequal=false;
						if(name1.charAt(i)>name2.charAt(i)){
							check=false;
							break;
						}
						if(name1.charAt(i)<name2.charAt(i)){
							check=true;
							break;
						}
					}
				}
				if(checkequal){
					check=true;
				}
				
			}
			else{//name2.length
				boolean checkequal=true;
				for(int i=0;i<minimum;i++){
					if(name1.charAt(i)!=name2.charAt(i)){
						checkequal=false;
						if(name1.charAt(i)>name2.charAt(i)){
							check=false;
							break;
						}
						if(name1.charAt(i)<name2.charAt(i)){
							check=true;
							break;
						}
					}
				}
				if(checkequal){
					check=false;
				}
			}
			return check;
		}
}

