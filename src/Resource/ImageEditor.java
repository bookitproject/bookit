package Resource;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.RoundRectangle2D;
import java.awt.image.BufferedImage;

/*
 * This class is for image editing, including:
 * 		return an image scaled to specified width and height
 * 		return a round-cropped image for displaying user's profile pic
 * 
 * Source for makeRoundedCorner():
 * 		http://stackoverflow.com/questions/7603400/how-to-make-a-rounded-corner-image-in-java
 */

public class ImageEditor {
	public static BufferedImage scaleImageSize(BufferedImage image, int width, int height) {
		BufferedImage newImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
	    Graphics g = newImage.createGraphics();
	    g.drawImage(image, 0, 0, width, height, null);
	    g.dispose();
	    return newImage;
	}
	
	/*public static BufferedImage scaleImageRatioSize(BufferedImage image, int width, int height) {
		BufferedImage newImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
	    Graphics g = newImage.createGraphics();
	    g.drawImage(image, 0, 0, width, height, null);
	    g.dispose();
	    return newImage;
	}*/
	
	public static BufferedImage makeRoundedCorner(BufferedImage image, int cornerRadius) {
		image = scaleImageSize(image, 80, 80);
	    int w = image.getWidth(); //c.getHeight();
	    int h = image.getHeight(); //image.getWidth()*h/image.getHeight();
	    BufferedImage output = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);

	    Graphics2D g2 = output.createGraphics();
	    g2.setComposite(AlphaComposite.Src);
	    g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
	    g2.setColor(Color.WHITE);
	    g2.fill(new RoundRectangle2D.Float(0, 0, w, h, cornerRadius, cornerRadius));

	    g2.setComposite(AlphaComposite.SrcAtop);
	    g2.drawImage(image, 0, 0, null);

	    g2.dispose();
	    
	    return output;
	}
	
}
