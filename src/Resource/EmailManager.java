package Resource;

import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class EmailManager {
	private final static String username = "bookitcontact";
	private final static String password = "jeffreymillerphd";
	private final static String fromEmail = "bookitcontact@gmail.com";
	private Session session;
	
	public EmailManager() {
		Properties prop = new Properties();
		prop.put("mail.smtp.auth", "true");
		prop.put("mail.smtp.starttls.enable", "true");
		prop.put("mail.smtp.host", "smtp.gmail.com");
		prop.put("mail.smtp.port", "587");

		this.session = Session.getDefaultInstance(prop, new Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});
	}
	
	public void sendForgotPasswordEmail(String toEmail, String user, String newPassword) {
		try {
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(fromEmail));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toEmail));
			message.setSubject("Bookit: forgot password");
			message.setText("Hello " + user + "! \n \n" + "Looks like you forgot your password.\nHere is a temporary password to log in: " 
							+ newPassword + "\n\nPlease remember to change it to your own in Edit Profile!" + "\n\nThanks, \nBookit Team");
			Transport.send(message);
		} catch(MessagingException me) {
			throw new RuntimeException(me);
		}
	}
	
	public void sendTextEmail(String toEmail, String fEmail, String user, String msg) {
		try {
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(fromEmail));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toEmail));
			message.setSubject("Bookit: You got a message from " + user);
			message.setText(msg + "\n\nIMPORTANT: To respond to this user, either go online or email: " + fEmail);
			Transport.send(message);
		} catch(MessagingException me) {
			throw new RuntimeException(me);
		}
	}
}
