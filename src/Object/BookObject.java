package Object;

import java.io.Serializable;
import java.util.Vector;

import javax.swing.ImageIcon;

public class BookObject implements Serializable {
	private static final long serialVersionUID = 1;
	
	private String title;
	private String author;
	private int edition;
	private float price;
	private String condition;
	private String courseType;
	private int courseNumber;
	private String username;
	private Vector<ImageIcon> images;
	
	public BookObject(String title, String author, int edition, float price,
			String cond, String courseType, int courseNumber, String username) {
		this.title = title;
		this.author = author;
		this.edition = edition;
		this.price = price;
		this.condition = cond;
		this.courseType = courseType;
		this.courseNumber = courseNumber;
		this.username = username;
		images = new Vector<ImageIcon>();
	}
	
	public String getTitle() {
		return title;
	}
	
	public String getAuthor() {
		return author;
	}
	
	public int getEdition() {
		return edition;
	}
	
	public float getPrice() {
		return price;
	}
	
	public String getCondition() {
		return condition;
	}
	
	public String getCourseType() {
		return courseType;
	}
	
	public int getCourseNumber() {
		return courseNumber;
	}
	
	public String getSellerUsername() {
		return username;
	}
	
	public void addImage(ImageIcon img) {
		images.add(img);
	}
	
	public Vector<ImageIcon> getImages() {
		return images;
	}
	
	public void setImages(Vector<ImageIcon> images) {
		this.images = images;
	}
}
