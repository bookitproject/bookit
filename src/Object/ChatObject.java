package Object;

import java.io.Serializable;

public class ChatObject implements Serializable {
	private static final long serialVersionUID = 1;
	
	private String senderName;
	private String receiverName;
	private String message;
	
	public ChatObject(String senderName, String receiverName, String message) {
		this.senderName = senderName;
		this.receiverName = receiverName;
		this.message = message;
	}

	public String getSenderName() {
		return senderName;
	}

	public String getReceiverName() {
		return receiverName;
	}

	public String getMessage() {
		return message;
	}
	
}
