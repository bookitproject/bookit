package Object;

import java.io.Serializable;

import javax.swing.ImageIcon;

/*
 * This object will be the object CLIENT sends to the server
 * Contains an description of client's request (String), a BookObject, a UserObject, a ChatObject
 */
public class MessageToServer implements Serializable {
	private static final long serialVersionUID = 1;
	
	private String mDescription;
	private BookObject mBook;
	private User mUser;
	private ChatObject mChat;
	
	// these are ONLY used for login authentication, checking if username exists
	private String mUsername; 
	private String mPassword;
	// mEmail is used ONLY for add email to a newly created user
	private String mEmail; 
	
	private String searchWord;
	private int criteria;
	private int sortCriteria;
	private ImageIcon icon;
	
	public MessageToServer(String descript, BookObject book, User user, ChatObject chat, String username, String pw, String email, String word, int crit, int sortC) {
		mDescription = descript;
		mBook = book;
		mUser = user;
		mChat = chat;
		mUsername = username;
		mPassword = pw;
		mEmail = email;
		searchWord = word;
		criteria = crit;
		sortCriteria = sortC;
	}
	
	public String getDescription() {
		return mDescription;
	}
	
	public BookObject getBook() {
		return mBook;
	}
	
	public User getUser() {
		return mUser;
	}
	
	public ChatObject getChat() {
		return mChat;
	}
	
	public String getUsername() {
		return mUsername;
	}
	
	public String getPassword() {
		return mPassword;
	}
	
	public String getEmail() {
		return mEmail;
	}
	
	public String getSearchWord() {
		return searchWord;
	}
	
	public int getCriteria() {
		return criteria;
	}
	
	public int getSortCriteria() {
		return sortCriteria;
	}

	public ImageIcon getIcon() {
		return icon;
	}

	public void setIcon(ImageIcon icon) {
		this.icon = icon;
	}

}
