package Object;

import java.io.Serializable;
import java.util.Vector;

import javax.swing.ImageIcon;

/*
 * This object will be the object SERVER sends to the client
 * Contains the same description client sent to the server (String), a Boolean
 */
public class MessageToClient implements Serializable {
	private static final long serialVersionUID = 1;
	
	private String mDescription;
	private Boolean mBool;
	private ChatObject mChat;
	private Vector<BookObject> mBooks;
	private BookObject book;
	private User mUser;
	private ImageIcon profilePic;
	
	public MessageToClient(String descript, Boolean bool, ChatObject chat, Vector<BookObject> books, User user) {
		mDescription = descript;
		mBool = bool;
		mChat = chat;
		mBooks = books;
		mUser = user;
		profilePic = null;
	}
	
	public String getDescription() {
		return mDescription;
	}
	
	public Boolean getBool() {
		return mBool;
	}
	
	public ChatObject getChat() {
		return mChat;
	}
	
	public Vector<BookObject> getBooks() {
		return mBooks;
	}
	
	public User getUser() {
		return mUser;
	}

	public BookObject getBook() {
		return book;
	}

	public void setBook(BookObject book) {
		this.book = book;
	}

	public ImageIcon getProfilePic() {
		return profilePic;
	}

	public void setProfilePic(ImageIcon profilePic) {
		System.out.println("before setting profile pic ");
		this.profilePic = profilePic;
		System.out.println("after setting profile pic " + this.profilePic.getIconHeight());
	}

}
