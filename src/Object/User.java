package Object;

import java.io.Serializable;

import Client.ChatDialog;

public class User implements Serializable {
	private static final long serialVersionUID = 1;
	
	private String username;
	private String password;
	//private ImageIcon profilePic;
	
	// for chat
	private ChatDialog chatDialog;
	
	public User(String username, String pw) {
		this.username = username;
		this.password = pw;
		chatDialog=null;
		// Instantiate user-specific GUIs
//		userPublicProfilePanel = new UserPublicProfilePanel(this);
//		userPrivateProfilePanel = new UserPrivateProfilePanel(this);
	}
	
	public String getUsername() {
		return username;
	}
	
	public void setUsername(String newname) {
		username = newname;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	
	// Get profile GUIs
//	public UserPublicProfilePanel getUserPublicProfilePanel() {
//		return userPublicProfilePanel;
//	}
//	public UserPrivateProfilePanel getUserPrivateProfilePanel() {
//		return userPrivateProfilePanel;
//	}
//	
	public ChatDialog getChatDialog() {
		return chatDialog;
	}
	
	public void setChatDialog(ChatDialog dialog){
		chatDialog=dialog;
	}
	
}
