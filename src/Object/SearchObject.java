package Object;

import java.io.Serializable;

public class SearchObject implements Serializable {
	private static final long serialVersionUID = 1;
	
	private String searchWord;
	private String seachCriteria;
	
	public SearchObject(String searchWord, String searchCri) {
		this.searchWord = searchWord;
		this.seachCriteria = searchCri;
	}
	
	public String getSearchWord() {
		return searchWord;
	}
	
	public String getSeachCriteria() {
		return seachCriteria;
	}
}
