package CustomGUI;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicComboBoxUI;

import Resource.ImageLibrary;

/*
 * Usage:
 * 		JComboBox.setUI(new BookitComboBoxUI());
		UIManager.put("ComboBox.foreground", Color.white);
        UIManager.put("ComboBox.background", new Color(0,0,0));   
        UIManager.put("ComboBox.selectionBackground",  new Color(0,0,0,0));
        UIManager.put("ComboBox.border", new RoundedCornerBorder());
 */

public class BookitComboBoxUI extends BasicComboBoxUI {
    public static ComponentUI createUI(JComponent c) {
        
    	return new BookitComboBoxUI();
    }

    protected JButton createArrowButton() {
    	Image downArrowImage = ImageLibrary.getImage("resource/img/icon/down_arrow.png");
    	TransparentButton button = new TransparentButton() {
    		private static final long serialVersionUID = 1;
			protected void paintComponent(Graphics g) {
				Graphics2D g2d = (Graphics2D) g.create();
				g2d.drawImage(downArrowImage, 0, 0, this.getWidth(), this.getHeight(), this);
				g2d.dispose();
				super.paintComponent(g);
			}  
	    };
	    return button;
    }
 }

