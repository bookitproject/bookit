package CustomGUI;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPasswordField;

public class RoundPasswordField extends JPasswordField{
	private static final long serialVersionUID = 1;

	public RoundPasswordField(){
		super();
		setOpaque(false);
    }
	
	protected void paintBorder(Graphics g) {
          g.setColor(Color.BLACK);
         g.drawRoundRect(0, 0, getWidth()-1, getHeight()-1, 20, 20);
    }
	
	 protected void paintComponent(Graphics g) {
		 int width = getWidth() - getInsets().left - getInsets().right;
	     int height = getHeight() - getInsets().top - getInsets().bottom;
		 g.setColor(Color.WHITE);
		 g.fillRoundRect(getInsets().left, getInsets().top, width, height, 20, 20);
		 super.paintComponent(g);
    }
}
