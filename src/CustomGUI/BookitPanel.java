package CustomGUI;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;

import javax.swing.JPanel;

import Resource.ImageLibrary;

public class BookitPanel extends JPanel {
	private static final long serialVersionUID = 1;
	
	private Image mImage;
	
	public BookitPanel() {
		super();
		
		mImage = ImageLibrary.getImage("resource/img/bg.jpg");
	}
	
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g.create();
		g2d.drawImage(mImage, 0, 0, this.getWidth(), this.getHeight(), this);
		g2d.dispose();
	}
	
	//**********DELETE*TEST***********
	public String getPanel() {
		return "";
	}
	//**********DELETE*TEST***********
}
