package CustomGUI;

import java.awt.Color;
import java.awt.Graphics;

public class CircleButton extends TransparentButton {

	private int i;
	private int selected;
	
	private static final long serialVersionUID = 1;
	public CircleButton(int i, int selected) {
		this.i = i;
		this.selected = 0;
	}
	
	public void setSelectedIndex(int selected) {
		this.selected = selected;
		paintComponent(getGraphics());
	}
	
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.setColor(Color.BLACK);
		if (i == selected) {
			g.setColor(Color.BLACK);
			g.fillOval(0, 0, 10, 10);
		} else {
			g.drawOval(0, 0, 10, 10);
		}
		this.setActionCommand("button"+i);
	}
}
