
package CustomGUI;
import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.*;
import java.awt.image.BufferedImage;
import java.util.Vector;
import java.awt.*;

public class EmojiTextPane extends JEditorPane {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Vector<ImageIcon> emoji;



	public EmojiTextPane() {
		super();
		createImage();
		this.setEditorKit(new StyledEditorKit());
		this.initListener();

	}

	public void initListener() {
		getDocument().addDocumentListener(new DocumentListener(){
			public void insertUpdate(DocumentEvent event) {
				final DocumentEvent e=event;
				SwingUtilities.invokeLater(new Runnable() {
					public void run() {
						if (e.getDocument() instanceof StyledDocument) {
							try {
								StyledDocument doc=(StyledDocument)e.getDocument();
								int start= Utilities.getRowStart(EmojiTextPane.this,Math.max(0,e.getOffset()-1));
								int end=Utilities.getWordStart(EmojiTextPane.this,e.getOffset()+e.getLength());
								//System.out.println(start + " " + (end-start));
								if (start < 0) start = 0;
								String text=doc.getText(start, end-start);

								for (int a=1;a<5;a++) {
									int i=text.indexOf("/"+a);
									while(i>=0) {
										final SimpleAttributeSet attrs=new SimpleAttributeSet(
												doc.getCharacterElement(start+i).getAttributes());
										if (StyleConstants.getIcon(attrs)==null) {
											//System.out.println("a is" + a);
											StyleConstants.setIcon(attrs, emoji.get(a-1));
											doc.remove(start+i, 2);
											doc.insertString(start+i,"/"+a, attrs);
										}
										i=text.indexOf("/"+a, i+2);
									}
								}
							} catch (BadLocationException e1) {
								e1.printStackTrace();
							}
						}
					}
				});
			}
			public void removeUpdate(DocumentEvent e) {
			}
			public void changedUpdate(DocumentEvent e) {
			}
		});
	}

	private void createImage() {
		emoji = new Vector<ImageIcon>();
		for (int i =1; i < 5;i++) {
			ImageIcon icon = new ImageIcon("resource/emoji/" +i+".png");
			Image img = icon.getImage() ;  
			Image newimg = img.getScaledInstance(19, 19,  java.awt.Image.SCALE_SMOOTH ) ;
			emoji.add(new ImageIcon(newimg));
		}
	}


	public void append(String s){
		try {
			Document document = this.getDocument();
			document.insertString(document.getLength(), s, null);
			//update();
		} catch(BadLocationException exc) {
			exc.printStackTrace();
		}

	}
	
	//manually update
	public void update() throws BadLocationException{
		StyledDocument doc=(StyledDocument)this.getDocument();
		String text = this.getText();
		int start = 0;
		for (int a=1;a<5;a++) {
			int i=text.indexOf("/"+a);
			while(i>=0) {
				final SimpleAttributeSet attrs=new SimpleAttributeSet(
						doc.getCharacterElement(start+i).getAttributes());
				if (StyleConstants.getIcon(attrs)==null) {
					StyleConstants.setIcon(attrs, emoji.get(a-1));
					doc.remove(start+i, 2);
					doc.insertString(start+i,"/"+a, attrs);
				}
				i=text.indexOf("/"+a, i+2);
			}
		}
		
	}


}