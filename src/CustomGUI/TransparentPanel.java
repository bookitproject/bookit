package CustomGUI;

import java.awt.LayoutManager;

import javax.swing.JPanel;

public class TransparentPanel extends JPanel {

	private static final long serialVersionUID = 1;

	public TransparentPanel() {
		setOpaque(false);
	}
	
	public TransparentPanel(LayoutManager layout) {
		setOpaque(false);
		setLayout(layout);
	}
}
