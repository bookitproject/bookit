package CustomGUI;

import java.awt.Color;
import java.awt.Graphics;

public class WhiteButton extends TransparentButton{
	private static final long serialVersionUID = 1;
	
	public WhiteButton(){
		setForeground(Color.WHITE);
	}
	
	protected void paintBorder(Graphics g) {
		    g.setColor(Color.WHITE);
		    g.drawRoundRect(0, 0, getWidth() - 1, getHeight() - 1,20,20);

	}
	 
}
