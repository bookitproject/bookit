package CustomGUI;

import javax.swing.JButton;

public class TransparentButton extends JButton {
	private static final long serialVersionUID = 1;

	public TransparentButton() {
		super();
		setBorderPainted(false); 
		setContentAreaFilled(false); 
		setFocusPainted(false); 
		setOpaque(false);
		//setCursor(new Cursor(Cursor.HAND_CURSOR));
	}
	
	public TransparentButton(String title) {
		super(title);
		setBorderPainted(false); 
		setContentAreaFilled(false); 
		setFocusPainted(false); 
		setOpaque(false);
		//setCursor(new Cursor(Cursor.HAND_CURSOR));
	}
}
