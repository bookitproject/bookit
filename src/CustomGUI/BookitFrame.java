package CustomGUI;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.GraphicsEnvironment;
import java.awt.Toolkit;
import java.io.File;
import java.io.IOException;
import java.util.Enumeration;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.UIManager;

public class BookitFrame extends JFrame {
	private static final long serialVersionUID = 1;
	
	private BookitPanel contentPane;
	private Font bookitFont;
	
	public BookitFrame(String title) {
		super(title);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
	
		setSize(Resource.Constants.frameWidth, Resource.Constants.frameHeight);
		
		setLocationRelativeTo(null);
		try {
			//load font
	    	bookitFont = Font.createFont(Font.TRUETYPE_FONT, new File("resource/font/Quicksand-Light.otf")).deriveFont(24f);    	
			GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
	        //register the font	
	        ge.registerFont(Font.createFont(Font.TRUETYPE_FONT, new File("resource/font/Quicksand-Light.otf")));
	        setUIFont(new javax.swing.plaf.FontUIResource(bookitFont));
		
		} catch (FontFormatException | IOException e) {
			System.out.println("FontFormatException: "+e.getMessage());
		}
		
		UIManager.put("Label.foreground", Color.WHITE); //***
		//UIManager.put("Panel.background", new Color(17, 12, 35));
		
		contentPane = new BookitPanel();
		setContentPane(contentPane);
//	    add(contentPane);
	    //setUndecorated(true);
	    
	}
	
	public JPanel getBookitContentPane() {
		return contentPane;
	}
	
	private static void setUIFont(javax.swing.plaf.FontUIResource f) {
	    Enumeration<Object> keys = UIManager.getDefaults().keys();
	    while (keys.hasMoreElements()) {
	        Object key = keys.nextElement();
	        Object value = UIManager.get(key);
	        if (value instanceof javax.swing.plaf.FontUIResource) {
	        	UIManager.put(key, f);
	        }
	    }
	}
	
}
