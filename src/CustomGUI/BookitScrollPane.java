package CustomGUI;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.RenderingHints;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.plaf.basic.BasicScrollBarUI;

import Resource.ImageLibrary;

/*
 * Usage example:
 * 		JTextPane resultpane = new JTextPane();
 * 		BookitScrollPane booksScrollPane = new BookitScrollPane((resultpane));
 * 		booksScrollPane.setPreferredSize(new Dimension(850,400));
 */

public class BookitScrollPane extends JScrollPane {
	private static final long serialVersionUID = 1;

	public BookitScrollPane(JComponent c) {
		super(c);
		
		JScrollBar sb = this.getVerticalScrollBar();
		sb.setUI(new BookitScrollBarUI());
		sb.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));
		sb.setOpaque(false);
		
		setOpaque(false);
		setBorder(null);
		//setViewportBorder(null);
		setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
	}
}

class BookitScrollBarUI extends BasicScrollBarUI {
	//@Override
	protected JButton createDecreaseButton(int orientation) {
		Image downScrollImage = ImageLibrary.getImage("resource/img/icon/up_scroll.png");
		TransparentButton down = new TransparentButton() {
	      private static final long serialVersionUID = 1;
	      protected void paintComponent(Graphics g) {
		    	super.paintComponent(g);
				Graphics2D g2d = (Graphics2D) g.create();
				g2d.drawImage(downScrollImage, 1, 0, this.getWidth()-1, this.getHeight(), this);
				g2d.dispose();
			}  
	      
	      public Dimension getPreferredSize() {
	    	  return new Dimension(this.getWidth(),this.getWidth());
	      }
	    };
	    return down;
	  }
	
	  @Override
	  protected JButton createIncreaseButton(int orientation) {
		  Image upScrollImage = ImageLibrary.getImage("resource/img/icon/down_scroll.png");
		  TransparentButton up = new TransparentButton() {
			  private static final long serialVersionUID = 1;
		      protected void paintComponent(Graphics g) {
			    	super.paintComponent(g);
					Graphics2D g2d = (Graphics2D) g.create();
					g2d.drawImage(upScrollImage, 1, 0, this.getWidth()-1, this.getHeight(), this);
					g2d.dispose();
			  }  
		      
		      public Dimension getPreferredSize() {
		    	  return new Dimension(this.getWidth(),this.getWidth());
		      }
		  };
		  return up;
	  }
	
	  @Override
	  protected void paintTrack(Graphics g, JComponent c, Rectangle r) {
	  }
	
	  @Override
	  protected void paintThumb(Graphics g, JComponent c, Rectangle r) {
		  Graphics2D g2 = (Graphics2D) g.create();
		  g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
	      Color color = Color.white;
	      JScrollBar sb = (JScrollBar) c;
  	      if (!sb.isEnabled() || r.width > r.height) {
  	    	  return;
	      } 
		  g2.setPaint(color);
		  g2.fillRoundRect(r.x+3, r.y+3, r.width-5, r.height-5, 10, 10);
		  g2.dispose();
	}
	
}