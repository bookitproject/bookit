package CustomGUI;

import java.awt.Color;
import java.awt.Graphics;


public class BlackButton extends TransparentButton{
	private static final long serialVersionUID = 1;
	
	public BlackButton(){
		setForeground(Color.BLACK);
		/*addMouseListener( new MouseAdapter(){
			public void mousePressed(MouseEvent me) {
				BlackButton.this.setBorder(BorderFactory.createBevelBorder(1));
		       	repaint();
		    }	   
		});*/
	}
	
	protected void paintBorder(Graphics g) {
		    g.setColor(Color.BLACK);
		    g.drawRoundRect(0, 0, getWidth() - 1, getHeight() - 1,20,20);

	}
	 
	
}
