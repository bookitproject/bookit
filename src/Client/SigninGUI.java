package Client;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import CustomGUI.BookitFrame;
import CustomGUI.BookitPanel;
import CustomGUI.RoundPasswordField;
import CustomGUI.RoundTextfiled;
import CustomGUI.TransparentButton;
import CustomGUI.TransparentPanel;
import Resource.ImageLibrary;
import Resource.MD5;

/*
 * Note: Line 150 for sending emails for forgot username/password
 * 		
 */
public class SigninGUI extends BookitFrame{

	private static final long serialVersionUID = 1L;

	private BookitPanel main_panel;
	private JLabel username_label,password_label;
	private RoundTextfiled username_tf;
	private RoundPasswordField password_tf;
	private TransparentButton signin,forgot,signup,backButton;
	private Client c;

	public SigninGUI(Client c) {
		super("Signin");
		this.c = c;
		instantiate();
		createGUI();
		addAction();
		setVisible(true);
	}

	private void instantiate() {
		main_panel = new BookitPanel();
		
		backButton = new TransparentButton();
		backButton.setPreferredSize(new Dimension(40,40));
		Image image = ImageLibrary.getImage("resource/img/icon/left_arrow.png");	
		image = image.getScaledInstance(40, 40, java.awt.Image.SCALE_SMOOTH) ; 
		backButton.setIcon(new ImageIcon(image));
		
		username_label = new JLabel("Username:");
		password_label = new JLabel("Password :");

		username_tf = new RoundTextfiled();
		username_tf.setPreferredSize(new Dimension(300,35));
		username_tf.setFont(new Font(username_tf.getFont().getName(),Font.PLAIN,35));
		password_tf = new RoundPasswordField();	
		password_tf.setPreferredSize(new Dimension(300,35));
		password_tf.setFont(new Font("Arial",Font.PLAIN,35));
		signin = new TransparentButton();
		signin.setBorder(BorderFactory.createEmptyBorder());
		ImageIcon icon = new ImageIcon("resource/img/login_signin.png");
		Image img = icon.getImage() ;  
		Image newimg = img.getScaledInstance(90, 90,  java.awt.Image.SCALE_SMOOTH ) ; 
		signin.setIcon(new ImageIcon(newimg));
		signin.setContentAreaFilled(false);
		signin.setToolTipText("SignIn");

		forgot = new TransparentButton();
		forgot.setText("Forgot Username/Password...?");

		forgot.setBorder(BorderFactory.createEmptyBorder());
		forgot.setFont(new Font(username_tf.getFont().getName(),Font.PLAIN,20));
		forgot.setContentAreaFilled(false);
		forgot.setForeground(new Color(80,70,70));
		forgot.setToolTipText("Retrieve Username/Password");

		signup = new TransparentButton();
		signup.setText("Signup now...?");

		signup.setBorder(BorderFactory.createEmptyBorder());
		signup.setFont(new Font(username_tf.getFont().getName(),Font.PLAIN,20));
		signup.setContentAreaFilled(false);
		signup.setForeground(new Color(80,70,70));
		signup.setToolTipText("SignUp");

	}

	private void createGUI(){
		setLayout(new BorderLayout());
		add(backButton,BorderLayout.NORTH);
		
		/*
		 * create the main sign in page
		 */
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.ipady = 10;
		main_panel.setLayout(new GridBagLayout());
		main_panel.add(username_label,gbc);
		main_panel.add(username_tf,gbc);
		gbc.gridy = 1;
		TransparentPanel blank=new TransparentPanel();
		blank.setPreferredSize(new Dimension(300,5));
		main_panel.add(blank,gbc);
		gbc.gridy = 2;
		main_panel.add(password_label,gbc);
		main_panel.add(password_tf,gbc);
		gbc.gridy = 3;
		gbc.gridwidth = 2;
		
		main_panel.add(forgot,gbc);
		gbc.gridy = 4;
		main_panel.add(signup,gbc);
		gbc.gridy = 5;
		main_panel.add(signin, gbc);

		add(main_panel,BorderLayout.CENTER);
	}

	private void addAction(){
		backButton.addActionListener(new ActionListener() { // sign in attempt
			@Override
			public void actionPerformed(ActionEvent arg0) {
				new HomePageGUI(c).setVisible(true);
				dispose();
			}
		});
		signin.addActionListener(new ActionListener() { // sign in attempt
			@Override
			public void actionPerformed(ActionEvent arg0) {
				userSignin();
			}
		});
		forgot.addActionListener(new ActionListener() { // open up the forgotpw frame
			@Override
			public void actionPerformed(ActionEvent arg0) {
				retrievePassword();
			}
		});
		signup.addActionListener(new ActionListener() { // sign in attempt
			@Override
			public void actionPerformed(ActionEvent arg0) {
				new SignupGUI(c);
				dispose();
			}
		});
		password_tf.addActionListener(signin.getActionListeners()[0]);


	}

	private void userSignin(){ // if the user enter tring to sign in 
		String username = username_tf.getText();
		String password = MD5.encrypt(new String(password_tf.getPassword()));

		if (username.isEmpty() || new String(password_tf.getPassword()).isEmpty()) {
			JOptionPane.showMessageDialog(this, "Username/Password cannot be empty!",
					"Error", JOptionPane.ERROR_MESSAGE);
		}
		else {	//client signin
			c.authenticateUser(username, password, this);
		}

	}

	private void retrievePassword(){ // if the user need the email to be sent
		String email = JOptionPane.showInputDialog(this, "Please enter a valid email address", 
				"Forgot Password/Username",JOptionPane.QUESTION_MESSAGE);

		if (email != null) 
			if (email.isEmpty()) {
				JOptionPane.showMessageDialog(this, "Email cannot be empty!",
						"Error", JOptionPane.ERROR_MESSAGE);
			}
			else{
				sendemail(email);
			}

	}

	private void sendemail(String e){
		c.forgotUsernamePw(e,this);
	}

	public void proceedToSearchPage(ImageIcon profilePic){
		new MainFrame(c, profilePic);
//		mf.setVisible(true);
		dispose();

	}

}
