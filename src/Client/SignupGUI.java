package Client;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import CustomGUI.BlackButton;
import CustomGUI.BookitFrame;
import CustomGUI.BookitPanel;
import CustomGUI.RoundPasswordField;
import CustomGUI.RoundTextfiled;
import CustomGUI.TransparentButton;
import CustomGUI.TransparentPanel;
import Resource.ImageLibrary;
import Resource.MD5;

/*
 * Note: Line 125 needs to be edited
 * 		Line 158 for forgot username/password and sending email
 */
public class SignupGUI extends BookitFrame{

	private static final long serialVersionUID = 1L;

	private BookitPanel main_panel;
	private JLabel username_label,password_label,repeat_label,email_label;
	private RoundTextfiled username_tf,email_tf;
	private RoundPasswordField password_tf,repeat_tf;
	private BlackButton signup;
	private TransparentButton backButton;
	private Client c;

	public SignupGUI(Client c) {
		super("SignUp");
		this.c = c;
		instantiate();
		createGUI();
		addAction();
		setVisible(true);
	}

	private void instantiate() {
		main_panel = new BookitPanel();
		username_label = new JLabel("Username:");
		password_label = new JLabel("Password :");
		repeat_label = new JLabel("Re-Type :");
		email_label = new JLabel("Email: ");
		
		backButton = new TransparentButton();
		backButton.setPreferredSize(new Dimension(40,40));
		Image image = ImageLibrary.getImage("resource/img/icon/left_arrow.png");	
		image = image.getScaledInstance(40, 40, java.awt.Image.SCALE_SMOOTH) ; 
		backButton.setIcon(new ImageIcon(image));
		
		username_tf = new RoundTextfiled();
		username_tf.setPreferredSize(new Dimension(300,35));
		username_tf.setFont(new Font(username_tf.getFont().getName(),Font.PLAIN,35));
		password_tf = new RoundPasswordField();	
		password_tf.setPreferredSize(new Dimension(300,35));
		password_tf.setFont(new Font("Arial",Font.PLAIN,35));
		repeat_tf = new RoundPasswordField();	
		repeat_tf.setPreferredSize(new Dimension(300,35));
		repeat_tf.setFont(new Font("Arial",Font.PLAIN,35));
		email_tf = new RoundTextfiled();
		email_tf.setPreferredSize(new Dimension(300,35));
		email_tf.setFont(new Font(username_tf.getFont().getName(),Font.PLAIN,35));
		
		signup = new BlackButton();
		signup.setText("SignUp!");
	}

	private void createGUI(){
		setLayout(new BorderLayout());
		add(backButton,BorderLayout.NORTH);
		/*
		 * create the main sign in page
		 */
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.ipady = 10;
		main_panel.setLayout(new GridBagLayout());
		main_panel.add(username_label,gbc);
		main_panel.add(username_tf,gbc);
		gbc.gridy = 1;
		TransparentPanel blank=new TransparentPanel();
		blank.setPreferredSize(new Dimension(300,5));
		main_panel.add(blank,gbc);
		gbc.gridy = 2;
		main_panel.add(password_label,gbc);
		main_panel.add(password_tf,gbc);
		gbc.gridy = 3;
		TransparentPanel blank1=new TransparentPanel();
		blank1.setPreferredSize(new Dimension(300,5));
		main_panel.add(blank1,gbc);
		gbc.gridy = 4;
		main_panel.add(repeat_label,gbc);
		main_panel.add(repeat_tf,gbc);
		gbc.gridy = 5;
		TransparentPanel blank2=new TransparentPanel();
		blank2.setPreferredSize(new Dimension(300,5));
		main_panel.add(blank2,gbc);
		gbc.gridy = 6;
		main_panel.add(email_label,gbc);
		main_panel.add(email_tf,gbc);
		gbc.gridy = 7;
		TransparentPanel blank3=new TransparentPanel();
		blank3.setPreferredSize(new Dimension(300,5));
		main_panel.add(blank3,gbc);	
		gbc.gridwidth = 2;
		gbc.gridy = 8;
		main_panel.add(signup, gbc);

		add(main_panel,BorderLayout.CENTER);
	}

	private void addAction(){
		backButton.addActionListener(new ActionListener() { // sign in attempt
			@Override
			public void actionPerformed(ActionEvent arg0) {
				new HomePageGUI(c).setVisible(true);
				dispose();
			}
		});
		signup.addActionListener(new ActionListener() { // sign in attempt
			@Override
			public void actionPerformed(ActionEvent arg0) {
				userSignUp();
			}
		});
		email_tf.addActionListener(signup.getActionListeners()[0]);
	}

	private void userSignUp(){ // sign up method
		String username = username_tf.getText();
		String password = MD5.encrypt(new String(password_tf.getPassword()));
		String repeat = MD5.encrypt(new String(repeat_tf.getPassword()));
		String email = email_tf.getText();
		
		if (username.isEmpty() || new String(password_tf.getPassword()).isEmpty()
				|| new String(repeat_tf.getPassword()).isEmpty()
				||email.isEmpty()) {
			JOptionPane.showMessageDialog(this, "Please fill up all the information!",
					"Error", JOptionPane.ERROR_MESSAGE);
		}
		else {	//check if password and repeat match
			if (password.equals(repeat)) {
				c.createUser(username, password, email, this);
			}
			else {
				JOptionPane.showMessageDialog(this, "Password do not match!!",
						"Error", JOptionPane.ERROR_MESSAGE);
			}

		}

	}

	public void proceedToSearchPage(){
		ImageIcon icon = new ImageIcon(ImageLibrary.getImage("resource/img/anon.jpg"));
		new MainFrame(c, icon);
		dispose();
	}
}
