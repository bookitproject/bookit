package Client;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout; 
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;

import CustomGUI.BlackButton;
import CustomGUI.BookitComboBoxUI;
import CustomGUI.BookitPanel;
import CustomGUI.BookitScrollPane;
import CustomGUI.RoundTextfiled;
import CustomGUI.TransparentPanel;
import Object.BookObject;
import Object.User;
import Resource.ImageLibrary;
import Resource.RoundedCornerBorder;

public class SearchPagePanel extends BookitPanel{

	/**
	 *  
	 */
	private static final long serialVersionUID = 1L;

	private TransparentPanel searchpane,sortpane,downpane;
	private BookitScrollPane booksScrollPane;
	//for search pane
	private JComboBox<String> searchComboBox;
	private RoundTextfiled searchinput;
	private BlackButton searchButton;
	//for sortpane
	private JLabel sort_label;
	private JComboBox<String> sortComboBox;
	//for resultpane
	private TransparentPanel resultpane,findnothing;
	private Vector<BookObject> searchedBooks;
	private User searchedUser;
	private String last_searched;
	private int prev_criteria;
	private ImageIcon profilepic;
	private TransparentPanel profilePicPanel;
	
	private Client c;
	private MainFrame Main_frame;
	
	
	public SearchPagePanel(MainFrame bf,Client c){
		this.c = c;
		this.Main_frame = bf;
		instantiate();
		createGUI();
		addAction();
		setVisible(true);

	}

	private void instantiate(){
		searchedBooks = new Vector<BookObject>();
		searchedUser = null;

		searchpane = new TransparentPanel();
		sortpane = new TransparentPanel();
		sortpane.setVisible(false);
		downpane = new TransparentPanel();
		downpane.setVisible(false);

		//for search pane
		searchComboBox = new JComboBox<String>() ;		
		searchComboBox.addItem("Title");
		searchComboBox.addItem("Class");
		searchComboBox.addItem("User");
		UIManager.put("ComboBox.foreground", Color.white);
		UIManager.put("ComboBox.background", new Color(0,0,0));   
		UIManager.put("ComboBox.selectionBackground",  new Color(0,0,0,0));
		UIManager.put("ComboBox.border", new RoundedCornerBorder());
		searchComboBox.setUI(new BookitComboBoxUI());
		searchComboBox.setPreferredSize(new Dimension(120,40));
		//searchComboBox.setFont(new Font(this.getFont().getName(),Font.PLAIN,20));


		searchinput = new RoundTextfiled();
		searchinput.setPreferredSize(new Dimension(550,40));
		searchinput.setFont(new Font(searchinput.getFont().getName(),Font.PLAIN,35));
		searchButton = new BlackButton();
		searchButton.setText("Search");
		searchButton.setPreferredSize(new Dimension(170,40));
		//searchButton.setFont(new Font(this.getFont().getName(),Font.PLAIN,35));

		//for sortpane
		sort_label = new JLabel("Sort by: ");
		sort_label.setPreferredSize(new Dimension(100,30));
		sort_label.setFont(new Font(searchButton.getFont().getName(),Font.PLAIN,20));
		sortComboBox = new JComboBox<String>();
		sortComboBox.addItem("Title");
		sortComboBox.addItem("Class");
		sortComboBox.addItem("Price");
		sortComboBox.addItem("Condition");
		sortComboBox.setUI(new BookitComboBoxUI());
		sortComboBox.setPreferredSize(new Dimension(130,30));
		sortComboBox.setFont(new Font(this.getFont().getName(),Font.PLAIN,20));

		//for resultpane	
		searchedUser = null;

		resultpane = new TransparentPanel();
		booksScrollPane = new BookitScrollPane(resultpane);
		booksScrollPane.setPreferredSize(new Dimension(850,400));
		booksScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);


		findnothing = new TransparentPanel();
		findnothing.setVisible(false);
		JLabel lb = new JLabel("Sorry, we are not able to find anything :(");
		findnothing.add(lb);
	}

	private void createGUI(){
		setLayout(new GridBagLayout());

		searchpane.add(searchComboBox);
		searchpane.add(searchinput);
		searchpane.add(searchButton);

		sortpane.add(sortComboBox);
		sortpane.add(sort_label);
		sortpane.add(sortComboBox);

		downpane.add(booksScrollPane);
		resultpane.setLayout(new GridLayout(0,1));
		downpane.add(findnothing);

		GridBagConstraints gbc = new GridBagConstraints();
		add(searchpane,gbc);
		gbc.gridy = 1;
		add(sortpane,gbc);
		gbc.gridy = 2;
		add(downpane,gbc);
	}	

	private void addAction(){
		searchButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (searchinput.getText().toString().isEmpty()) { // if no input
					JOptionPane.showMessageDialog(Main_frame, "Please enter a search term");
				}
				else {
					int criteria = searchComboBox.getSelectedIndex()+1;
					if (criteria == 2) { // if search by course name
						if (valid_coursename(searchinput.getText().toLowerCase()) == false){
							JOptionPane.showMessageDialog(Main_frame, "Format incorrect! e.g. Math 126.");
							return;
						}
					}
					last_searched = searchinput.getText().toLowerCase();

					findnothing.setVisible(false);

					prev_criteria = searchComboBox.getSelectedIndex()+1;
					int sort = sortComboBox.getSelectedIndex();
					search(searchinput.getText().toLowerCase(),prev_criteria,sort+1);
				}
			}
		});

		sortComboBox.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent arg0) {
				int sort = sortComboBox.getSelectedIndex();
				search(last_searched,prev_criteria,sort+1);
				updateResult(0);
			}
		});
		
		searchinput.addActionListener(searchButton.getActionListeners()[0]);
	}

	/*
	 * searching
	 */
	private void search(String s,int criteria, int sort) {
		if (criteria == 3) {
			c.getUserSearch(s, this);
		}
		else {
			c.getBookSearch(s, criteria,sort,this);
		}
		
	
	}



	private void updateResult(int type){
		/*
		 * 0 = update books, 1 = update user
		 */

		if (type == 0){ // if return books
			if (searchedBooks.size() == 0){
				sortpane.setVisible(false);
				downpane.setVisible(false);
				findNothing();	
			}
			else {
				downpane.setVisible(true);
				booksScrollPane.setVisible(true);
				sortpane.setVisible(true);
				resultpane = new TransparentPanel();
				resultpane.setLayout(new GridLayout(0,1));
				resultpane.setVisible(true);
				booksScrollPane.setViewportView(resultpane);
				for (int i = 0; i < searchedBooks.size(); i++){
					BookObject book = searchedBooks.get(i);
					TransparentPanel t = new TransparentPanel();

					t.setLayout(new GridLayout(0,1));
					JLabel title = new JLabel(book.getTitle());
					title.setForeground(Color.black);
					title.setFont(new Font(title.getFont().getName(),Font.BOLD,35));
					JLabel price = new JLabel("$ "+book.getPrice());
					price.setForeground(Color.black);
					JLabel author = new JLabel("By "+book.getAuthor());
					author.setForeground(Color.black);
					JLabel condition = new JLabel("Book's condition: "+book.getCondition());
					condition.setForeground(Color.black);
					JLabel course_name = new JLabel(book.getCourseType()
							+"-"+book.getCourseNumber());
					course_name.setForeground(Color.black);
					JLabel seller = new JLabel("Seller: "+ book.getSellerUsername());
					seller.setForeground(Color.black);

					t.add(title);t.add(author);
					t.add(price);t.add(condition);
					t.add(course_name);t.add(seller);
					Border border = LineBorder.createBlackLineBorder();
					t.setBorder(border);
					t.addMouseListener(new MouseListener() {
						@Override
						public void mouseClicked(MouseEvent e) {
							BookInfoPanel new_panel = new BookInfoPanel(book,Main_frame,c);
							MainFrame.visitedPanels.add(new_panel);		
							Main_frame.refreshPanel();
						}

						@Override
						public void mouseEntered(MouseEvent e) {
							Border redBorder = BorderFactory.createLineBorder(new Color(171,136,225),5);
					        t.setBorder(redBorder);
						}

						@Override
						public void mouseExited(MouseEvent e) {
							Border blackBorder = BorderFactory.createLineBorder(Color.BLACK);
					        t.setBorder(blackBorder);
						}

						@Override
						public void mousePressed(MouseEvent e) {
						}

						@Override
						public void mouseReleased(MouseEvent e) {
						}
					});
					t.setVisible(true);
					resultpane.add(t);				
				}
				downpane.revalidate();
				downpane.repaint();
			}
		}
		else{ // if return user
			if (searchedUser == null){
				sortpane.setVisible(false);
				findNothing();	
			}
			else {
				sortpane.setVisible(false);
				resultpane = new TransparentPanel();
				resultpane.setLayout(new GridLayout(0,1));
				booksScrollPane.setViewportView(resultpane);
				downpane.setVisible(true);
				booksScrollPane.setVisible(true);
				resultpane.setVisible(true);
				
				TransparentPanel t = new TransparentPanel();
				profilepic = null;
				JLabel name = new JLabel(searchedUser.getUsername());
				name.setForeground(Color.BLACK);
				name.setFont(new Font(name.getFont().getName(),Font.BOLD,35));
				
				/*
				 *  initialize the profile pic of searchedUser
				 *  and send request to get pic from server
				 */
				profilePicPanel = new TransparentPanel();
				
				profilePicPanel.add(new JLabel(profilepic));
				c.getSearchedUserProfilePic(searchedUser.getUsername(),SearchPagePanel.this);
								
				Border border = LineBorder.createBlackLineBorder();
				t.add(profilePicPanel);
				t.add(name);
				t.setBorder(border);
				resultpane.add(t);
				t.setVisible(true);
				t.addMouseListener(new MouseListener() {
					@Override
					public void mouseClicked(MouseEvent e) {
						UserPublicProfilePanel new_panel = new UserPublicProfilePanel(c,Main_frame,searchedUser,profilepic);
						MainFrame.visitedPanels.add(new_panel);				
						Main_frame.refreshPanel();
					}

					@Override
					public void mouseEntered(MouseEvent e) {
						Border redBorder = BorderFactory.createLineBorder(new Color(171,136,225),5);
				        t.setBorder(redBorder);
					}

					@Override
					public void mouseExited(MouseEvent e) {
						Border blackBorder = BorderFactory.createLineBorder(Color.BLACK);
				        t.setBorder(blackBorder);
					}

					@Override
					public void mousePressed(MouseEvent e) {

					}

					@Override
					public void mouseReleased(MouseEvent e) {

					}
				});
				
				downpane.revalidate();
				downpane.repaint();
			}
		}
		
	}

	private void findNothing(){
		downpane.setVisible(true);
		resultpane.setVisible(false);
		sortpane.setVisible(false);
		booksScrollPane.setVisible(false);
		findnothing.setVisible(true);
	
	}

	public void deliverBooks(Vector<BookObject> books){
		searchedBooks = books;
		updateResult(0);
	}

	public void getUser(User u){
		searchedUser = u;
		updateResult(1);
	}

	private boolean valid_coursename(String s){
		if (s.contains(" ")== true){
			if (s.matches(".*\\d+.*")){
//			if (s.matches("\\D+ \\s* \\d+")) {
				return true;
			}
		}
		return false;
	}
	
	public void refreshsearching(){
		if (searchinput.getText().isEmpty()) {
			int sort = sortComboBox.getSelectedIndex();
			if (!searchinput.getText().isEmpty())
			search(searchinput.getText().toLowerCase(),prev_criteria,sort+1);
		}
	}
	
	
	/*
	 * Client call this to update searched user's pic
	 */
	public void UpdateSeachedUserProfilePic(ImageIcon icon){
		profilepic = icon;
		Image image = icon.getImage();
		image = image.getScaledInstance(100, 100, java.awt.Image.SCALE_SMOOTH);
		BufferedImage buff = ImageLibrary.getRenderedImage(image);
		profilePicPanel.remove(0);
		profilePicPanel.add(new JLabel(new ImageIcon(buff)));
		profilePicPanel.revalidate();
		profilePicPanel.repaint();
		
	}
	
	
	//**********DELETE*TEST***********
	public String getPanel() {
		return "SearchlPagesPanel";
	}
	//**********DELETE*TEST***********
	
}
