package Client;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.SwingConstants;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

import CustomGUI.BlackButton;
import CustomGUI.BookitPanel;
import CustomGUI.TransparentButton;
import CustomGUI.TransparentPanel;
import Resource.ImageEditor;
import Resource.ImageLibrary;
import Resource.MD5;

public class EditProfilePanel extends BookitPanel {
	private static final long serialVersionUID = 1L;
	private TransparentPanel main;
	private TransparentPanel left_panel; 
	private TransparentPanel picture_panel;
	private BlackButton upload_button;

	private TransparentButton backButton;

	private TransparentPanel right_panel;
	private BlackButton changepw_button;
	//private TransparentPanel changingpw_panel;

	private BlackButton changeemail_button;

	private Client c;
	private MainFrame Main_frame;
	//private BookitPanel p;
	private NavBar nav;

	public EditProfilePanel(Client c,MainFrame mf){
		this.c = c;
		//this.p = p;
		Main_frame = mf;
		this.nav = Main_frame.getNavBar();
	
		instantiate();
		createGUI();
		addAction();
		setVisible(true);
	}

	private void instantiate(){
		left_panel = new TransparentPanel();
		picture_panel = new TransparentPanel();
		
		//Image profilePic = ImageLibrary.getImage("resource/img/loveme.jpg"); //***TESTING*DELETE***
		Image profilePic = nav.returnProfilepic();//get the profile pic from the nav bar
		profilePic = profilePic.getScaledInstance(200, 200, java.awt.Image.SCALE_SMOOTH) ; 
		picture_panel.add(new JLabel(new ImageIcon(profilePic)));

		Border border = LineBorder.createBlackLineBorder();
		picture_panel.setBorder(border);
		upload_button = new BlackButton();
		upload_button.setText("Upload...");
		upload_button.setFont(new Font(upload_button.getFont().getName(),Font.PLAIN,20));;


		right_panel = new TransparentPanel();
		changepw_button = new BlackButton();
		changepw_button.setText("Change Password");
		changepw_button.setFont(new Font(changepw_button.getFont().getName(),Font.PLAIN,20));;
		changepw_button.setPreferredSize(new Dimension(250,40));
		changepw_button.setHorizontalTextPosition(SwingConstants.CENTER);
		changeemail_button = new BlackButton();
		changeemail_button.setText("Change Email");
		changeemail_button.setFont(changepw_button.getFont());
		changeemail_button.setPreferredSize(new Dimension(250,40));
		
		backButton = new TransparentButton();
		backButton.setPreferredSize(new Dimension(40,40));
		Image image = ImageLibrary.getImage("resource/img/icon/left_arrow.png");	
		image = image.getScaledInstance(40, 40, java.awt.Image.SCALE_SMOOTH) ; 
		backButton.setIcon(new ImageIcon(image));
	}

	private void createGUI(){
		setLayout(new BorderLayout());

		main = new TransparentPanel();
		main.setLayout(new GridLayout(1,0));
		add(backButton,BorderLayout.NORTH);
		main.add(left_panel); main.add(right_panel);
		add(main,BorderLayout.CENTER);

		GridBagConstraints gbc = new GridBagConstraints();

		left_panel.setLayout(new GridBagLayout());
		
		left_panel.add(picture_panel,gbc);
		gbc.gridheight = 30;
		gbc.gridy = 1;
		left_panel.add(upload_button,gbc);


		right_panel.setLayout(new GridBagLayout());
		gbc = new GridBagConstraints();
		gbc.ipady = 20;
		gbc.ipadx = 100;
		gbc.gridwidth = 50;
		right_panel.add(changepw_button,gbc);
		gbc.gridy = 1;
		TransparentPanel blank=new TransparentPanel();
		blank.setPreferredSize(new Dimension(300,15));
		right_panel.add(blank,gbc);
		gbc.gridy=2;
		right_panel.add(changeemail_button,gbc);
	}

	private void addAction(){
		backButton.addActionListener(new ActionListener(){	
			public void actionPerformed(ActionEvent ae){
				MainFrame.visitedPanels.pop();
				
				Main_frame.refreshPanel();
			}
		});

		upload_button.addActionListener(new ActionListener(){	
			public void actionPerformed(ActionEvent ae){
				JFileChooser filechooser = new JFileChooser();
				filechooser.setPreferredSize(new Dimension(500,300)); 
				filechooser.setFont(null);
				filechooser.setDialogTitle("Upload Image...");   
				FileFilter filter = new FileNameExtensionFilter( "Image files", ImageIO.getReaderFileSuffixes());
				filechooser.setFileFilter(filter);
				filechooser.setMultiSelectionEnabled(false);

				if(filechooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION){
					File file = filechooser.getSelectedFile();
					Image myPicture = null;
					myPicture = ImageLibrary.getImage(file.getAbsolutePath());
					ImageIcon i = new ImageIcon(myPicture);
					
//					nav.setProfileImage(i);
//					nav.revalidate();
//					nav.repaint();
					
					//send to server
					c.uploadImage(i, c.getUser().getUsername(), EditProfilePanel.this);
					
					//if uploaded, client call:
					// updateimage(i);								
				}
			}
		});

		changepw_button.addActionListener(new ActionListener(){	
			public void actionPerformed(ActionEvent ae){
				backButton.setVisible(false);
				showChangepwPanel();
			}
		});

		changeemail_button.addActionListener(new ActionListener(){	
			public void actionPerformed(ActionEvent ae){
				String email = JOptionPane.showInputDialog(Main_frame, "Please enter a valid email address", 
						"Change Email address",JOptionPane.QUESTION_MESSAGE);
				if (email != null) {
					if (email.isEmpty()) {
						JOptionPane.showMessageDialog(Main_frame, "Email cannot be empty!",
								"Error", JOptionPane.ERROR_MESSAGE);
					}
					else {
						if (!email.contains("@")){
							JOptionPane.showMessageDialog(Main_frame, "Email is not valid!",
									"Error", JOptionPane.ERROR_MESSAGE);
						}
						else {
							//send to server
							c.updateEmail(c.getUser().getUsername(), email,EditProfilePanel.this);
						}
					}
				}
			}
		});
	}

	private void showChangepwPanel(){
		BlackButton cancel = new BlackButton();
		cancel.setText("Cancel");
		BlackButton ok = new BlackButton();
		ok.setText("Confirm");
		JLabel oldpassword_label = new JLabel("Original password  ");
		JPasswordField oldpassword_tf = new JPasswordField();
		oldpassword_tf.setPreferredSize(new Dimension(500,30));
		oldpassword_tf.setFont(new Font("Arial",Font.PLAIN,35));
		JLabel newpassword_label = new JLabel("New password  ");
		JPasswordField newpassword_tf = new JPasswordField();
		newpassword_tf.setPreferredSize(new Dimension(500,30));
		newpassword_tf.setFont(new Font("Arial",Font.PLAIN,35));
		JLabel repeatpassword_label = new JLabel("Re-enter password  ");
		JPasswordField repeatpassword_tf = new JPasswordField();
		repeatpassword_tf.setPreferredSize(new Dimension(500,30));
		repeatpassword_tf.setFont(new Font("Arial",Font.PLAIN,35));

		TransparentPanel changingpw = new TransparentPanel();
		changingpw.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.ipady = 10;
		changingpw.add(oldpassword_label,gbc);
		changingpw.add(oldpassword_tf,gbc);
		gbc.gridy = 1;
		changingpw.add(newpassword_label,gbc);
		changingpw.add(newpassword_tf,gbc);
		gbc.gridy = 2;
		changingpw.add(repeatpassword_label,gbc);
		changingpw.add(repeatpassword_tf,gbc);
		gbc.gridwidth = 2;
		gbc.gridy = 3;
		changingpw.add(ok, gbc);
		gbc.gridy = 4;
		changingpw.add(cancel, gbc);
		main.setVisible(false);
		
		add(changingpw,BorderLayout.CENTER);

		ok.addActionListener(new ActionListener(){	
			public void actionPerformed(ActionEvent ae){
				// check if everything is valid
				String old_pw = MD5.encrypt(new String(oldpassword_tf.getPassword()));
				String new_pw = MD5.encrypt(new String(newpassword_tf.getPassword()));
				String repeat_pw = MD5.encrypt(new String(repeatpassword_tf.getPassword()));
			
				
				if (old_pw.isEmpty() || new String(newpassword_tf.getPassword()).isEmpty() ||
						new String(repeatpassword_tf.getPassword()).isEmpty() ) {
					JOptionPane.showMessageDialog(Main_frame, "Please fill up all the information!",
							"Error", JOptionPane.ERROR_MESSAGE);
				}
				else {	
					if (c.getUser().getPassword().equals(old_pw)) {
						if (new_pw.equals(repeat_pw)){		
							//send to server
							c.updatePw(c.getUser().getUsername(), new_pw);
							remove(changingpw);
							instantiate();
							createGUI();
							addAction();
							revalidate();
							repaint();
						}
						else {
							JOptionPane.showMessageDialog(Main_frame, "Passwords do not match!",
									"Error", JOptionPane.ERROR_MESSAGE);
						}
					}
					else {
						JOptionPane.showMessageDialog(Main_frame, "The original password does not match",
								"Error", JOptionPane.ERROR_MESSAGE);
					}

				}


			}
		});
		cancel.addActionListener(new ActionListener(){	
			public void actionPerformed(ActionEvent ae){	
				remove(changingpw);
				instantiate();
				createGUI();
				addAction();
				revalidate();
				repaint();
			}
		});
	}

	//Client calls this after uploaded
	public void updateimage(ImageIcon profilePic){
		Image image = profilePic.getImage();
		image = image.getScaledInstance(200, 200, java.awt.Image.SCALE_SMOOTH);
		BufferedImage buff = ImageLibrary.getRenderedImage(image);
		ImageIcon icon = new ImageIcon(buff);
		BufferedImage masked = ImageEditor.makeRoundedCorner(buff, 100);
		picture_panel.remove(0);
		picture_panel.add(new JLabel(icon));
		picture_panel.revalidate();
		picture_panel.repaint();
		icon = new ImageIcon(masked);
		nav.setProfileImage(icon);
		nav.revalidate();
		nav.repaint();
	}
	
	//**********DELETE*TEST***********
	public String getPanel() {
		return "EditProfilePanel";
	}
	//**********DELETE*TEST***********

}
