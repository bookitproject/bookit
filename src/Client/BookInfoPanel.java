package Client;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.border.Border;

import CustomGUI.BlackButton;
import CustomGUI.BookitPanel;
import CustomGUI.CircleButton;
import CustomGUI.TransparentButton;
import CustomGUI.TransparentPanel;
import Object.BookObject;
import Object.User;
import Resource.Constants;
import Resource.ImageLibrary;

public class BookInfoPanel extends BookitPanel implements ActionListener{
	
	private static final long serialVersionUID = 1;
    private static final Border border = BorderFactory.createEmptyBorder(10,10,10,10);
    private BookObject book; 
    private JLabel imageLabel;
    private JLabel titleLabel;
    private BlackButton contactButton, wishButton;
    private BlackButton prevButton, nextButton;
    private TransparentButton backButton;
    private JPanel leftPanel, rightPanel, controlPanel, imagePanel, circlePanel, bottomPanel;
    private JTable bookTable;
    private int currIndex;
    private Vector<CircleButton> buttons;
    private MainFrame mf;
    private Client c;

	//for loading
	private ImageIcon loadIcon = new ImageIcon("resource/loading.gif");
	private SplashScreen loadingScreen;
	
    //p = previously visited panel
    //private BookitPanel p;
    
    public BookInfoPanel(BookObject book, MainFrame mf, Client c) {
    	this.mf = mf;
    	this.c = c;
    	//this.p = p;
    	currIndex = -1;
		c.getImages(book);
		BookObject newBook = c.getBook();			
		this.book = newBook;
		c.setBookToNull();
    	initializeVariables();
    	createGUI();
    	addActions();
    	setVisible(true);
    }
    
    private void initializeVariables() {
    	imageLabel = new JLabel();
    	imageLabel.setHorizontalAlignment(JLabel.CENTER);
    	
    	if (book.getImages() != null) {
	    	if (book.getImages().size() == 0) {
	    		imageLabel.setText("IMAGE NOT AVAILABLE");
	    	} else {
	    		imageLabel.setText("");
	    		currIndex = 0;
	    		ImageIcon icon = book.getImages().get(0);
	        	imageLabel.setIcon(icon);
	    	}
    	} else {
    		imageLabel.setText("IMAGE NOT AVAILABLE");
    	}
    	
    	prevButton = new BlackButton();
    	prevButton.setPreferredSize(new Dimension(50, 80));
    	prevButton.setText("<");
        prevButton.setActionCommand("prev");
        
    	nextButton = new BlackButton();
    	nextButton.setPreferredSize(new Dimension(50, 80));
    	nextButton.setText(">");
        nextButton.setActionCommand("next");
        
    	titleLabel = new JLabel();
		titleLabel.setText(book.getTitle());
		titleLabel.setHorizontalAlignment(JLabel.CENTER);
		titleLabel.setOpaque(false);
		titleLabel.setBorder(border);
		
		leftPanel = new TransparentPanel(new GridBagLayout());
		leftPanel.setBorder(border);
	    rightPanel = new TransparentPanel(new GridBagLayout());
	    rightPanel.setBorder(border);
	    controlPanel = new TransparentPanel(new BorderLayout());
	    controlPanel.setBorder(border);
	    
	    imagePanel = new TransparentPanel(new BorderLayout());
	    
	    bookTable = new JTable(6, 2);
	    bookTable.setRowHeight(30);
	    bookTable.setValueAt("Author", 0, 0);
	    bookTable.setValueAt("Edition", 1, 0);
	    bookTable.setValueAt("Price", 2, 0);
	    bookTable.setValueAt("Condition", 3, 0);
	    bookTable.setValueAt("Course Name", 4, 0);
	    bookTable.setValueAt("Seller", 5, 0);
	    bookTable.setValueAt(book.getAuthor(), 0, 1);
	    bookTable.setValueAt(book.getEdition(), 1, 1);
	    bookTable.setValueAt("$"+book.getPrice(), 2, 1);
	    bookTable.setValueAt(book.getCondition(), 3, 1);
	    bookTable.setValueAt(book.getCourseType() + "-" + book.getCourseNumber(), 4, 1);
	    bookTable.setValueAt(book.getSellerUsername(), 5, 1);
	    bookTable.setDefaultEditor(Object.class, null);
	    
	    bottomPanel = new TransparentPanel(new FlowLayout());
	    contactButton = new BlackButton();
	    contactButton.setText("Contact Seller");
	    contactButton.setActionCommand("contact");
	    wishButton = new BlackButton();
	    wishButton.setText("Add to Wishlist");
	    wishButton.setActionCommand("wishlist");
	    
	    if(c.getUser() == null) {
	    	contactButton.setEnabled(false);
	    	wishButton.setEnabled(false);
	    }
	    
	    circlePanel = new TransparentPanel(new GridBagLayout());
	    buttons = new Vector<CircleButton>();
	    for (int i = 0; i < book.getImages().size(); i++) {
	    	CircleButton button = new CircleButton(i, 0);
	    	button.addActionListener(this);
	    	buttons.addElement(button);
	    	circlePanel.add(button);
	    }
	    
	    backButton = new TransparentButton();
		backButton.setPreferredSize(new Dimension(40,40));
		Image image = ImageLibrary.getImage("resource/img/icon/left_arrow.png");	
		image = image.getScaledInstance(40, 40, java.awt.Image.SCALE_SMOOTH) ; 
		backButton.setIcon(new ImageIcon(image));
	    backButton.setActionCommand("back");
}
    
    private void createGUI() {
    	setLayout(new BorderLayout());
    	setSize(Constants.frameWidth, Constants.frameHeight);
    	
    	imagePanel.add(titleLabel, BorderLayout.NORTH);
    	imagePanel.add(imageLabel, BorderLayout.CENTER);
    	imagePanel.add(circlePanel, BorderLayout.SOUTH);
    	imagePanel.add(leftPanel, BorderLayout.WEST);
    	imagePanel.add(rightPanel, BorderLayout.EAST);
    	leftPanel.add(prevButton);
        rightPanel.add(nextButton);
        TransparentPanel blank2=new TransparentPanel();
		blank2.setPreferredSize(new Dimension(150,20));
        bottomPanel.add(wishButton);
        bottomPanel.add(blank2);
        bottomPanel.add(contactButton);
        controlPanel.add(bookTable, BorderLayout.CENTER);
        controlPanel.add(bottomPanel, BorderLayout.SOUTH);
        add(backButton, BorderLayout.NORTH);
        add(imagePanel, BorderLayout.CENTER);
        add(controlPanel, BorderLayout.SOUTH);
    }
    
    private void addActions() {
    	prevButton.addActionListener(this);
    	nextButton.addActionListener(this);
    	backButton.addActionListener(this);
    	wishButton.addActionListener(this);
    	contactButton.addActionListener(this);
    }
    
    public void actionPerformed(ActionEvent ae) {
        String cmd = ae.getActionCommand();
        if (cmd.equals("back")) {
//        	c.setBookToNull();
        	
        	MainFrame.visitedPanels.pop();			
        	mf.refreshPanel();
        }
        if (cmd.equals("contact")) {
        	String name = book.getSellerUsername();
        	//show loading
    		loadingScreen = new SplashScreen(loadIcon);
        	c.getSellerAndProfilePic(name, BookInfoPanel.this);
        	return;
        }
        if (cmd.equals("wishlist")) {
        	c.addBookWL(book);
        	JOptionPane.showMessageDialog(this, 
					book.getTitle()+" has been added to your wish list!",
					"Add Success",
					JOptionPane.INFORMATION_MESSAGE);
        	return;
        }
        if ("next".equals(cmd)) {
        	if (currIndex == -1) return;
        	currIndex++;
        	if (currIndex == book.getImages().size()) {
        		currIndex = 0;
        	}
        	ImageIcon icon = book.getImages().get(currIndex);
        	if (icon != null) {
        		imageLabel.setText("");
        		imageLabel.setIcon(icon);
        	}
            else imageLabel.setText("Image not available.");
        	imageLabel.revalidate();
        	imageLabel.repaint();
        	
        	buttons.get(currIndex).setSelectedIndex(currIndex);
    		for (int i = 0; i < buttons.size(); i++) {
    			if (i != currIndex) {
    				buttons.get(i).setSelectedIndex(-1);
    			}
    		}
    		circlePanel.revalidate();
    		circlePanel.repaint();
    		return;
        }
        if ("prev".equals(cmd)) {
        	if (currIndex == -1) return;
        	currIndex--;
        	if (currIndex < 0) {
        		currIndex = book.getImages().size()-1;
        	}
        	ImageIcon icon = book.getImages().get(currIndex);
        	if (icon != null) {
        		imageLabel.setText("");
        		imageLabel.setIcon(icon);
        	}
            else imageLabel.setText("Image not available.");
        	imageLabel.revalidate();
        	imageLabel.repaint();
        	
        	buttons.get(currIndex).setSelectedIndex(currIndex);
    		for (int i = 0; i < buttons.size(); i++) {
    			if (i != currIndex) {
    				buttons.get(i).setSelectedIndex(-1);
    			}
    		}
    		circlePanel.revalidate();
    		circlePanel.repaint();
    		return;
        }
        for (int i = 0; i < book.getImages().size(); i++) {
        	if (cmd.equals("button"+i)) {
        		currIndex = i;
        		buttons.get(i).setSelectedIndex(i);
        		ImageIcon icon = book.getImages().get(currIndex);
            	imageLabel.setIcon(icon);
            	imageLabel.revalidate();
            	imageLabel.repaint();
            	circlePanel.revalidate();
        		circlePanel.repaint();
        	}
        	else if (cmd.contains("button")) {
        		buttons.get(i).setSelectedIndex(-1);
        		circlePanel.revalidate();
        		circlePanel.repaint();
        	}
        }
    }

    public void contactSeller(ImageIcon icon,User user){
    	UserPublicProfilePanel u = new UserPublicProfilePanel(c,mf,user,icon);
		MainFrame.visitedPanels.add(u);				
		mf.refreshPanel();
		loadingScreen.dispose();
    }

}
