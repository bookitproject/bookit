package Client;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;

import CustomGUI.BlackButton;
import CustomGUI.BookitPanel;
import CustomGUI.BookitScrollPane;
import CustomGUI.TransparentButton;
import CustomGUI.TransparentPanel;
import Object.BookObject;
import Object.User;
import Resource.ImageEditor;
import Resource.ImageLibrary;

public class UserPublicProfilePanel extends BookitPanel {
	private static final long serialVersionUID = 1;
	
	private User searchedUser;
	private boolean isSearchedUserOnline = false;
	
	private Client mClient;
	private MainFrame mMainFrame;

	private JPanel topPanel;
	private TransparentButton backButton; 
	private JLabel profileLabel;
	private TransparentPanel profilePicPanel;
	private BlackButton contactButton;
	private ImageIcon icon;
	private JPanel mainPanel;
	private JLabel sellLabel;
	private JTable slListTable;
	private DefaultTableModel slListTableModel;
	private BookitScrollPane sScrollPane;
	
	//store selllist and wishlist BookObjects
	private Vector<BookObject> slBookObjects;
	
	/*
	 * [top] topPanel
	 * 	=> user profile label
	 * 	=> chat button
	 * [main] sellBookPanel
	 * 	=> sellBookLabel
	 * 	=> JList of books to sell
	 */
	
	//*************************TESTING*EDIT***************************
	public UserPublicProfilePanel(Client client, MainFrame mf, User searchedUser,ImageIcon icon) {
	//need to pass in pred panel for back button
	//*************************TESTING*EDIT***************************
		this.mClient = client;
		this.mMainFrame = mf;
		this.icon = icon;
		this.searchedUser = searchedUser;
		
		//check if online;
		mClient.checkOnline(searchedUser.getUsername(), this);
		
		instantiateVariables();
		createGUI();
		addActionListeners();
	}
	
	private void instantiateVariables() {
		topPanel = new TransparentPanel();
		backButton = new TransparentButton();
		Image bbImage = ImageLibrary.getImage("resource/img/icon/left_arrow.png");
		backButton.setIcon(new ImageIcon(bbImage));
		//*************************TESTING*EDIT***************************
		profileLabel = new JLabel(searchedUser.getUsername());
		//*************************TESTING*EDIT***************************
		
		//profile panel***
		profilePicPanel = new TransparentPanel();
		
		//Image profilePic = ImageLibrary.getImage("resource/img/sample_profilePic.jpg"); //***TESTING*DELETE***
		//Image profilePic = mClient.getUser().getProfilePic();
		Image profilePic = icon.getImage();
		
		BufferedImage masked = ImageLibrary.getRenderedImage(profilePic);
		masked = ImageEditor.scaleImageSize(masked, 100, 100);
		profilePicPanel.add(new JLabel(new ImageIcon(masked)));
		
		contactButton = new BlackButton();
		contactButton.setText("Contact");
		contactButton.setPreferredSize(new Dimension(300,80));
		//if visitor, disable contact button
		if(mClient.getUser() == null) contactButton.setEnabled(false);
		
		mainPanel = new TransparentPanel();
		sellLabel = new JLabel("Selling Book List");
		
		String slColumn[] = {"Book title"};
		slListTable = new JTable();
		slListTableModel = new DefaultTableModel(slColumn,0);
		slListTable.setDefaultEditor(Object.class, null);
		slListTable.setModel(slListTableModel);
		slListTable.setRowHeight(35);
		slListTable.setRowMargin(10);
		sScrollPane = new BookitScrollPane(slListTable);
		sScrollPane.setBorder(BorderFactory.createEmptyBorder());
		sScrollPane.setPreferredSize(new Dimension(850,400));
		
		mClient.getSellBooksPub(searchedUser.getUsername(), this);
	}
	
	private void createGUI() {
		//topPanel.setLayout(new /*GridLayout(2,1)*/ BoxLayout(topPanel, BoxLayout.Y_AXIS));
		JPanel topmostPanel = new TransparentPanel();
		topmostPanel.setLayout(new GridLayout(1,6));
		topmostPanel.add(backButton);
		topmostPanel.add(profilePicPanel);
		topmostPanel.add(profileLabel);
		topPanel.add(topmostPanel);
		topPanel.add(contactButton);
		add(topPanel, BorderLayout.NORTH);
		
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
		mainPanel.add(sellLabel);
		mainPanel.add(sScrollPane);
		add(mainPanel, BorderLayout.CENTER);
	}
	
	private void addActionListeners() {
		backButton.addActionListener(new ActionListener() { 
			@Override
			public void actionPerformed(ActionEvent arg0) {
				MainFrame.visitedPanels.pop();				
				mMainFrame.refreshPanel();
			}
		});
		
		contactButton.addActionListener(new ActionListener() { 
			public void actionPerformed(ActionEvent arg0) {
				mClient.checkOnline(searchedUser.getUsername(),UserPublicProfilePanel.this);
				if(mClient.getUser().getUsername().equals(searchedUser.getUsername())){
					JOptionPane.showMessageDialog(UserPublicProfilePanel.this,
							"you can`t talk to yourself",
							"WARNIGN",
							JOptionPane.ERROR_MESSAGE);
				}
				
				else{
					if(isSearchedUserOnline) {
						System.out.println("sfsgv");
						if(mClient.getUser().getChatDialog()==null){
							System.out.println("no window");
							mMainFrame.chat.doClick();

							mMainFrame.getDialog().add_tab(searchedUser.getUsername());
							mClient.add_chat(searchedUser.getUsername());
							mClient.getUser().setChatDialog(mMainFrame.getDialog());
						}else{
							mMainFrame.chat.doClick();
							if(mClient.getUser().getChatDialog().exsit(searchedUser.getUsername())){
								System.out.println("exsit");
								mMainFrame.getDialog().set_select(searchedUser.getUsername());
							}else{
								System.out.println("not exsit");
								mMainFrame.getDialog().add_tab(searchedUser.getUsername());
								mClient.add_chat(searchedUser.getUsername());
							}
						}

						
					} 
					else {
						String value = JOptionPane.showInputDialog(UserPublicProfilePanel.this,
								"the user is offline, please enter your message, we will send to the user`s email",
							    "OFFLINE WARNING",
							 JOptionPane.QUESTION_MESSAGE);
						if(value!=null){
							mClient.sendOfflineMessage(searchedUser.getUsername(),value,mClient.getUser().getUsername());
						}
                        
					}
				
				}

			}
		});
		
		slListTable.addMouseListener(new MouseAdapter() {
		    public void mouseClicked(MouseEvent evt) {
		    	JTable table =(JTable) evt.getSource();
		        Point p = evt.getPoint();
		    	int selectedRow = table.rowAtPoint(p);
		        if (evt.getClickCount() == 2) { // Double-click detected
		        	if(selectedRow != -1) {
		            	String booktitle = slListTable.getValueAt(selectedRow, 0).toString();
            	
		            	System.out.println(booktitle+ " at "+selectedRow+" got clicked!");
		            	
		            	//Vector<BookObject>
		            	BookInfoPanel new_panel = new BookInfoPanel(slBookObjects.get(selectedRow), mMainFrame, mClient);
						MainFrame.visitedPanels.add(new_panel);				
						mMainFrame.refreshPanel();
		            	
		            	//c.getBook(booktitle, c.getUser().getUsername(), UserPrivateProfilePanel.this);
		            }
		        }
		    }
		});
		
	}
	
	
	public void updateSearchedUserIsOnline(boolean isOnline) {
		isSearchedUserOnline = isOnline;
	}
	
	//called in client when a book on a list is selected
	/*public void showBook(BookObject book) {
		BookInfoPanel new_panel = new BookInfoPanel(book, mMainFrame, mClient);
		MainFrame.visitedPanels.add(new_panel);				
		mMainFrame.refreshPanel();
	}*/
	
	public void populateSellBookList(Vector<BookObject> books) {
		slListTableModel.setRowCount(0);
		if (books == null) return;
		for (BookObject b : books) {
			String[] newBook = {b.getTitle()};
			slListTableModel.addRow(newBook);
			System.out.println(b.getTitle());
		}
		
		slBookObjects = books;
	}
	
	//**********DELETE*TEST***********
	public String getPanel() {
		return "UserPublicProfilePanel";
	}
	//**********DELETE*TEST***********
	
	/*************************TESTING*DELETE***************************
	public static void main(String[] args) {
		BookitFrame bif = new BookitFrame("Test for UserPublicProfilePanel");
		bif.add(new UserPublicProfilePanel(null, null, new User("Jeffrey Miller PhD", null)), BorderLayout.CENTER);
		bif.setVisible(true);
	}
	//*************************TESTING*DELETE***************************/

}
