package Client;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.HeadlessException;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Vector;

import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

import CustomGUI.BlackButton;
import CustomGUI.BookitPanel;
import CustomGUI.RoundTextfiled;
import CustomGUI.TransparentButton;
import CustomGUI.TransparentPanel;
import Object.BookObject;
import Resource.ImageLibrary;

public class AddBookPanel extends BookitPanel{
	private static final long serialVersionUID = 1;
    private TransparentButton backButton;
	private JLabel AddBook;
    private JPanel upperbutton,upperbutton_blank;
    private BlackButton finished;//finish button
    private JPanel upperpanel;//for addbook label and information
    private JPanel upperwrap; //for wrapping backButton and addABookLabel
    private JPanel upper_lower_panel;//for information
    private JLabel book_title;
    private JLabel auther;
    private JLabel edition;
    private JLabel condition;
    private JLabel price;
    private JLabel class_book_course;
    private JLabel class_book_type;
    private RoundTextfiled book_title_field;
    private RoundTextfiled auther_field;
    private RoundTextfiled edition_field;
    private JComboBox<String> condition_field;
    private RoundTextfiled price_field;
    private RoundTextfiled class_book_field;
    private RoundTextfiled class_book_number_field;
    private JLabel upload;
    private BlackButton choose;
    private JPanel info_upper;//for textfield
    private JPanel left_info;//left_info
    private JPanel right_info;//right_info
    private JPanel left_book_title;
    private JPanel left_auther;
    private JPanel left_edition;
    private JPanel right_condtion;
    private JPanel right_price;
    private JPanel right_class;
    private JPanel image_panel;
    private JPanel blank_panel;
    private JPanel image_icon_panel;
    private	JScrollPane scrollPane;//for image
    private Client client;
    private String username;
    private JLabel image_label;
    private Vector<ImageIcon> image_book;
    private MainFrame mMainFrame;
    
	public AddBookPanel(Client c, MainFrame mf) {
		super();
		client = c;
		mMainFrame = mf;
		
		username = client.getUser().getUsername();
		setup();
		addaction();
		setVisible(true);
	}
	
	private void setup(){		
		upperbutton = new TransparentPanel();
		upperbutton_blank = new TransparentPanel();
		upperbutton_blank.setPreferredSize(new Dimension(1024,10));
		upperbutton.setLayout(new BorderLayout());
		upperbutton.add(upperbutton_blank,BorderLayout.NORTH);
		
		upperwrap = new TransparentPanel();
		upperwrap.setLayout(new BoxLayout(upperwrap, BoxLayout.X_AXIS));
		backButton = new TransparentButton();
		backButton.setPreferredSize(new Dimension(40,40));
		Image image = ImageLibrary.getImage("resource/img/icon/left_arrow.png");	
		image = image.getScaledInstance(40, 40, java.awt.Image.SCALE_SMOOTH) ; 
		backButton.setIcon(new ImageIcon(image));
		upperwrap.add(backButton);
		
		AddBook = new JLabel("ADD A BOOK");
		upperwrap.add(AddBook);
		upperbutton.add(upperwrap,BorderLayout.CENTER);
		image_label = new JLabel();
		image_book = new Vector<ImageIcon>();
		book_title = new JLabel("BOOK TITLE");
		//book_title.setFont(new Font(book_title.getFont().getName(),Font.PLAIN,35));
		auther = new JLabel("AUTHOR");
		//auther.setFont(new Font(auther.getFont().getName(),Font.PLAIN,35));
		edition = new JLabel("EDITION");
		//edition.setFont(new Font(edition.getFont().getName(),Font.PLAIN,35));
		condition = new JLabel("CONDITION");
		//condition.setFont(new Font(condition.getFont().getName(),Font.PLAIN,35));
		price = new JLabel("PRICE");
		//price.setFont(new Font(price.getFont().getName(),Font.PLAIN,35));
		class_book_course = new JLabel("CLASS");
		//class_book_course.setFont(new Font(class_book_course.getFont().getName(),Font.PLAIN,35));
		class_book_type = new JLabel("NUMBER");
		//class_book_type.setFont(new Font(class_book_type.getFont().getName(),Font.PLAIN,35));
		
		book_title_field = new RoundTextfiled();
		book_title_field.setPreferredSize(new Dimension(310,30));
		//book_title_field.setFont(new Font(book_title_field.getFont().getName(),Font.PLAIN,25));
		auther_field = new RoundTextfiled();
		auther_field.setPreferredSize(new Dimension(350,30));
		//auther_field.setFont(new Font(auther_field.getFont().getName(),Font.PLAIN,25));
		edition_field = new RoundTextfiled();
		edition_field.setPreferredSize(new Dimension(350,30));
		//edition_field.setFont(new Font(edition_field.getFont().getName(),Font.PLAIN,25));
		String options[] = {"excellent", "good", "okay","poor"};
		condition_field = new JComboBox<String>(options);
		condition_field.setPreferredSize(new Dimension(210,30));
		//condition_field.setFont(new Font(condition_field.getFont().getName(),Font.PLAIN,25));
		price_field = new RoundTextfiled();
		price_field.setPreferredSize(new Dimension(284,30));
		//price_field.setFont(new Font(price_field.getFont().getName(),Font.PLAIN,25));
		class_book_field = new RoundTextfiled();
		class_book_field.setPreferredSize(new Dimension(80,30));
		//class_book_field.setFont(new Font(class_book_field.getFont().getName(),Font.PLAIN,25));
		class_book_number_field = new RoundTextfiled();
		class_book_number_field.setPreferredSize(new Dimension(80,30));
		//class_book_number_field.setFont(new Font(class_book_number_field.getFont().getName(),Font.PLAIN,25));
		
		left_book_title = new TransparentPanel();
		left_auther = new TransparentPanel();
		left_edition = new TransparentPanel();
		right_condtion = new TransparentPanel();
		right_price = new TransparentPanel();
		right_class = new TransparentPanel();
		left_book_title.setLayout(new FlowLayout());
		left_auther.setLayout(new FlowLayout());
		left_edition.setLayout(new FlowLayout());
		right_condtion.setLayout(new FlowLayout());
		right_price.setLayout(new FlowLayout());
		right_class.setLayout(new FlowLayout());
		
		left_book_title.add(book_title);
		left_book_title.add(book_title_field);
		left_auther.add(auther);
		left_auther.add(auther_field);
		left_edition.add(edition);
		left_edition.add(edition_field);
		right_condtion.add(condition);
		right_condtion.add(condition_field);
		right_price.add(price);
		right_price.add(price_field);
		right_class.add(class_book_course);
		right_class.add(class_book_field);
		right_class.add(class_book_type);
		right_class.add(class_book_number_field);
		
		
		left_info = new TransparentPanel(); 
		right_info = new TransparentPanel();
		left_info.setLayout(new BoxLayout(left_info, BoxLayout.Y_AXIS));
		left_info.setPreferredSize(new Dimension(500,200));
		right_info.setLayout(new BoxLayout(right_info, BoxLayout.Y_AXIS));
		right_info.setPreferredSize(new Dimension(500,200));
		left_info.add(left_book_title);
		left_info.add(left_auther);
		left_info.add(left_edition);
		right_info.add(right_price);
		right_info.add(right_condtion);
		right_info.add(right_class);
		
		
		info_upper = new TransparentPanel();
		info_upper.setLayout(new BorderLayout());
		info_upper.add(left_info,BorderLayout.WEST);
		info_upper.add(right_info,BorderLayout.EAST);
		
		upload=new JLabel("UPLOAD THE IMAGE");
		
		blank_panel = new TransparentPanel();
		blank_panel.setPreferredSize(new Dimension(500,30));
		choose = new BlackButton(); 
		choose.setText("CHOOSE FILE FROM COMPUTER");
		//choose.setFont(new Font(choose.getFont().getName(),Font.PLAIN,30));
		
		image_icon_panel = new JPanel();
		image_icon_panel.setBackground(new Color(224,224,224));
		image_icon_panel.setLayout(new FlowLayout());
		scrollPane = new JScrollPane(image_icon_panel);
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		scrollPane.setPreferredSize(new Dimension(getWidth()/2,200));
		image_panel = new TransparentPanel();
		image_panel.setLayout(new BorderLayout());
		image_panel.add(blank_panel,BorderLayout.NORTH);
		TransparentPanel blank1=new TransparentPanel();
		blank1.setLayout(new FlowLayout());
		TransparentPanel blank2=new TransparentPanel();
		blank2.setPreferredSize(new Dimension(10,20));
		blank1.add(blank2);
		blank1.add(choose);
		image_panel.add(blank1,BorderLayout.CENTER);
		image_panel.add(image_label,BorderLayout.SOUTH);
		
	
		
		
		upper_lower_panel = new TransparentPanel();
		upper_lower_panel.setLayout(new BorderLayout());
		upper_lower_panel.add(info_upper,BorderLayout.NORTH);
		TransparentPanel blank5=new TransparentPanel();
		blank5.setLayout(new FlowLayout());
		TransparentPanel blank6=new TransparentPanel();
		blank6.setPreferredSize(new Dimension(10,20));
		blank5.add(blank6);
		blank5.add(upload);
		upper_lower_panel.add(blank5, BorderLayout.CENTER);
		upper_lower_panel.add(image_panel, BorderLayout.SOUTH);
		
		upperpanel = new TransparentPanel();
		upperpanel.setLayout(new BorderLayout());
		upperpanel.add(upperbutton,BorderLayout.NORTH);
		upperpanel.add(upper_lower_panel,BorderLayout.SOUTH);
		finished=new BlackButton();
		finished.setText("FINISHED");
		TransparentPanel blank3=new TransparentPanel();
		blank3.setLayout(new FlowLayout());
		TransparentPanel blank4=new TransparentPanel();
		blank4.setPreferredSize(new Dimension(10,20));
		blank3.add(blank4);
		blank3.add(finished);
		this.setLayout(new BorderLayout());
		this.add(blank3,BorderLayout.SOUTH);
		this.add(upperpanel,BorderLayout.NORTH);
		
	}
	
	private void addaction(){
		backButton.addActionListener(new ActionListener(){	
			public void actionPerformed(ActionEvent ae){
				MainFrame.visitedPanels.pop();			
				mMainFrame.refreshPanel();
			}
		});
		
		choose.addActionListener(new ActionListener(){	
			public void actionPerformed(ActionEvent ae){
				final Font font = new Font("Verdana", Font.PLAIN, 11);
				JFileChooser filechooser= new JFileChooser() {
					private static final long serialVersionUID = 1;
					protected JDialog createDialog(Component parent) throws HeadlessException {
		                JDialog dialog = super.createDialog(parent);
		                recursivelySetFonts(dialog, font);
		                return dialog;
		        }};
		        filechooser.setPreferredSize(new Dimension(500,300)); 
				filechooser.setFont(null);
				filechooser.setDialogTitle("Open File...");   
				FileFilter filter = new FileNameExtensionFilter( "Image files", ImageIO.getReaderFileSuffixes());
				filechooser.setFileFilter(filter);
				filechooser.setMultiSelectionEnabled(true);
				if(filechooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION){
					File [] files = filechooser.getSelectedFiles();
					Image myPicture = null;
					String fileTexts = "";
					for (File file : files) {
						myPicture = ImageLibrary.getImage(file.getAbsolutePath());
						ImageIcon icon = new ImageIcon(myPicture);
						image_book.add(icon);
						fileTexts += " " + file.getName();
					}
					image_label.setText(fileTexts);									
				}
			}
		});
		
		finished.addActionListener(new ActionListener(){	
			public void actionPerformed(ActionEvent ae){		
				String title = book_title_field.getText();
				String author = auther_field.getText();
				int edition = Integer.parseInt(edition_field.getText());
				float price = Float.parseFloat(price_field.getText());
				String condition = (String)condition_field.getSelectedItem();
				
				String coursetype = class_book_field.getText();
				int coursenumber = Integer.parseInt(class_book_number_field.getText());
				if((int)(Math.log10(coursenumber)+1)!=3){
					
				}
				BookObject add_book = new BookObject(title,author,edition,price,condition,coursetype,coursenumber,username);
				add_book.setImages(image_book);
				System.out.println(add_book.getTitle());
				client.addBook(add_book);
				
				MainFrame.visitedPanels.pop();			
				mMainFrame.refreshPanel();
			}
		});		
	}
	
	public static void recursivelySetFonts(Component comp, Font font) {
        comp.setFont(font);
        if (comp instanceof Container) {
            Container cont = (Container) comp;
            for(int j = 0, ub = cont.getComponentCount(); j<ub; ++j)
                recursivelySetFonts(cont.getComponent(j), font);
        }
    }
}
