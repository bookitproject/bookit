package Client;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Image;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;

import CustomGUI.BlackButton;
import CustomGUI.BookitPanel;
import CustomGUI.BookitScrollPane;
import CustomGUI.TransparentButton;
import CustomGUI.TransparentPanel;
import Object.BookObject;
import Resource.ImageLibrary;

public class UserPrivateProfilePanel extends BookitPanel {
	private static final long serialVersionUID = 1;

	private Client c;
	private MainFrame main;
	
	private TransparentButton backButton, addBookButton, sDeleteButton, wlDeleteButton;
	private BlackButton editButton;
	
//	private JList<String> sList;
//	private DefaultListModel<String> sListModel; 
	private JTable slListTable;
	private DefaultTableModel slListTableModel;
	private BookitScrollPane sScrollPane;
	
	private JTable wlListTable;
	private DefaultTableModel wlListTableModel;
	private BookitScrollPane wlScrollPane;
	
	//store selllist and wishlist BookObjects
	private Vector<BookObject> slBookObjects;
	private Vector<BookObject> wlBookObjects;
	
	//p = previously visited panel
    //private BookitPanel p;
	
	public UserPrivateProfilePanel(Client c,MainFrame main) {
		super();
		//this.p = p;
		this.c=c;
		this.main=main;
		initializeVariables();
		createGUI();
		addActionListeners();
		setVisible(true);
		
		slBookObjects = new Vector<BookObject>();
		wlBookObjects = new Vector<BookObject>();
	}
	
	private void initializeVariables() {
		backButton = new TransparentButton();
		backButton.setPreferredSize(new Dimension(40,40));
		Image image = ImageLibrary.getImage("resource/img/icon/left_arrow.png");	
		image = image.getScaledInstance(40, 40, java.awt.Image.SCALE_SMOOTH) ; 
		backButton.setIcon(new ImageIcon(image));
		
		editButton = new BlackButton();
		editButton.setText("Edit");
		
//		sListModel = new DefaultListModel<String>();
//		sList = new JList<String>(sListModel);
		String slColumn[] = {"Book title"};
		slListTable = new JTable();
		slListTableModel = new DefaultTableModel(slColumn,0);
		slListTable.setDefaultEditor(Object.class, null);
		slListTable.setModel(slListTableModel);
		slListTable.setRowHeight(35);
		slListTable.setRowMargin(10);
		sScrollPane = new BookitScrollPane(slListTable);
		sScrollPane.setBorder(BorderFactory.createEmptyBorder());

		
		sDeleteButton = new TransparentButton();
		sDeleteButton.setPreferredSize(new Dimension(40,40));
		image = ImageLibrary.getImage("resource/img/icon/delete.png");
		image = image.getScaledInstance(40, 40, java.awt.Image.SCALE_SMOOTH) ; 
		sDeleteButton.setIcon(new ImageIcon(image));
		wlDeleteButton = new TransparentButton();
		wlDeleteButton.setPreferredSize(new Dimension(40,40));
		wlDeleteButton.setIcon(new ImageIcon(image));
		
		addBookButton = new TransparentButton();
		addBookButton.setPreferredSize(new Dimension(40,40));
		image = ImageLibrary.getImage("resource/img/icon/add.png");
		image = image.getScaledInstance(40, 40, java.awt.Image.SCALE_SMOOTH) ; 
		addBookButton.setIcon(new ImageIcon(image));		
		
		String wlColumn[] = {"Book title", "Seller"};
		wlListTable = new JTable();
		wlListTableModel = new DefaultTableModel(wlColumn,0);
		wlListTable.setDefaultEditor(Object.class, null);
		wlListTable.setModel(wlListTableModel);
		wlListTable.setRowHeight(35);
		wlListTable.setRowMargin(10);
		wlScrollPane = new BookitScrollPane(wlListTable);
		wlScrollPane.setBorder(BorderFactory.createEmptyBorder());
		
	}
	
	private void createGUI() {
		this.setLayout(new BorderLayout());
		JPanel topPanel = new TransparentPanel();
		topPanel.setLayout(new FlowLayout());	
		topPanel.add(backButton);
		
		JLabel nameLabel = new JLabel(c.getUser().getUsername());
		//JLabel nameLabel = new JLabel("Username");
		
		topPanel.add(nameLabel);
		topPanel.add(editButton);
		
		// Sell list
		JPanel middlepanel= new TransparentPanel();
		middlepanel.setLayout(new BorderLayout());
		JPanel middleTopPanel = new TransparentPanel();
		middleTopPanel.setLayout(new FlowLayout());	
		JLabel sLabel = new JLabel("MY SELLING BOOK LIST");
		middleTopPanel.add(sLabel);
		middleTopPanel.add(addBookButton);
		middleTopPanel.add(sDeleteButton);
		slListTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		//sList.setVisibleRowCount(-1);
		
		c.getSellBooksPriv(c.getUser().getUsername(), this);
		
		sScrollPane.setPreferredSize(new Dimension(800,200));
		sScrollPane.setVerticalScrollBarPolicy(BookitScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		middlepanel.add(middleTopPanel, BorderLayout.NORTH);
		middlepanel.add(sScrollPane, BorderLayout.CENTER);
		
		// Wish list
		JPanel wlPanel = new TransparentPanel();
		wlPanel.setLayout(new BoxLayout(wlPanel,BoxLayout.Y_AXIS));
		JPanel wlTopPanel = new TransparentPanel();
		wlTopPanel.setLayout(new FlowLayout());
		JLabel wlLabel = new JLabel("MY WISHLIST");
		wlTopPanel.add(wlLabel);
		wlTopPanel.add(wlDeleteButton);
		wlListTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		c.getWLBooks(c.getUser().getUsername(), this);
		
		wlPanel.add(wlTopPanel);
		wlScrollPane.setPreferredSize(new Dimension(800,200));
		wlScrollPane.setVerticalScrollBarPolicy(BookitScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		wlPanel.add(wlScrollPane);
		
		add(topPanel,BorderLayout.NORTH);
		add(middlepanel,BorderLayout.CENTER);
		add(wlPanel,BorderLayout.SOUTH);
	}
	
	private void addActionListeners() {
		backButton.addActionListener(new ActionListener() { 
			@Override
			public void actionPerformed(ActionEvent ae) {			
				System.out.println("Back button fired in UserPrivateProfilePanel");
				
				MainFrame.visitedPanels.pop();			
				main.refreshPanel();
			}
		});
		
		editButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				EditProfilePanel new_panel = new EditProfilePanel(c, main);
				MainFrame.visitedPanels.add(new_panel);
				
				main.refreshPanel();
			}
		});
		
		addBookButton.addActionListener(new ActionListener() { 
			@Override
			public void actionPerformed(ActionEvent ae) {
				AddBookPanel new_panel = new AddBookPanel(c, main);
				MainFrame.visitedPanels.add(new_panel);
				
				main.refreshPanel();
			}
		});
		
		sDeleteButton.addActionListener(new ActionListener() { 
			public void actionPerformed(ActionEvent ae) {
				int selectedRow = slListTable.getSelectedRow();
				if (selectedRow != -1) {
					String booktitle = slListTable.getValueAt(selectedRow, 0).toString();
					
					c.deleteBook(booktitle, c.getUser().getUsername(), UserPrivateProfilePanel.this);
					
					//Vector<BookObject>
					System.out.println(":  " + selectedRow + " of " + slBookObjects.size());
					slBookObjects.remove(selectedRow);
				}
				
			}
		});	
				
		wlDeleteButton.addActionListener(new ActionListener() { 
			public void actionPerformed(ActionEvent ae) {
				int selectedRow = wlListTable.getSelectedRow();
		        if (selectedRow != -1) { 
		        	String booktitle = wlListTable.getValueAt(selectedRow, 0).toString();
		        	String sellername = wlListTable.getValueAt(selectedRow, 1).toString();
		        	
		        	c.deleteBookWL(booktitle, sellername,UserPrivateProfilePanel.this);			
		            
		            //wlListTableModel.removeRow(selectedRow); //***
		        	
		        	//Vector<BookObject>
					wlBookObjects.remove(selectedRow);
		        }
			}
		});

		slListTable.addMouseListener(new MouseAdapter() {
		    public void mouseClicked(MouseEvent evt) {
		    	JTable table =(JTable) evt.getSource();
		        Point p = evt.getPoint();
		    	int selectedRow = table.rowAtPoint(p);
		        if (evt.getClickCount() == 2) { // Double-click detected
		        	if(selectedRow != -1) {
		            	String booktitle = slListTable.getValueAt(selectedRow, 0).toString();

		            	
		            	System.out.println(booktitle+ " at "+selectedRow+" got clicked!");
		            	
		            	//Vector<BookObject>
		            	BookInfoPanel new_panel = new BookInfoPanel(slBookObjects.get(selectedRow), main, c);
						MainFrame.visitedPanels.add(new_panel);				
						main.refreshPanel();
		            	
		            	//c.getBook(booktitle, c.getUser().getUsername(), UserPrivateProfilePanel.this);
		            }
		        }
		    }
		});
		
		wlListTable.addMouseListener(new MouseAdapter() {
		    public void mouseClicked(MouseEvent evt) {
		    	JTable table =(JTable) evt.getSource();
		        Point p = evt.getPoint();
		    	int selectedRow = table.rowAtPoint(p);
		    	if (evt.getClickCount() == 2) { // Double-click detected
		    		if(selectedRow != -1) {
		    			String booktitle = wlListTable.getValueAt(selectedRow, 0).toString();
		    			String sellername = wlListTable.getValueAt(selectedRow, 1).toString();
		    			
		    			System.out.println(booktitle+ " "+sellername+" at "+selectedRow+" got clicked!");
		    			
		    			//Vector<BookObject>
		    			BookInfoPanel new_panel = new BookInfoPanel(wlBookObjects.get(selectedRow), main, c);
						MainFrame.visitedPanels.add(new_panel);				
						main.refreshPanel();
		    			
		    			//c.getBook(booktitle, sellername, UserPrivateProfilePanel.this);
		    		}
		    	}
		    }
		});
		
	}
	
	/*called in client when a book on a list is selected
	public void showBook(BookObject book) {
		System.out.println("UserPrivateProfile showBook: "+book.getTitle());
		main.showBookInfoPanel(new BookInfoPanel(book, main, c, this));
	}*/
	
	public void populateSellBookList(Vector<BookObject> books) {
		slListTableModel.setRowCount(0);
		if (books == null) {
			System.out.println("books is null!!!!!");
			return;
		}

		for (BookObject b : books) {
			String[] newBook = {b.getTitle()};
			slListTableModel.addRow(newBook);
			System.out.println(b.getTitle());
		}
		
		slBookObjects = books;
	}
	
	public void populateWL(Vector<BookObject> books) {
		wlListTableModel.setRowCount(0);
		if (books == null) return;
		for (BookObject b : books) {
			String[] newBook = {b.getTitle(), b.getSellerUsername()};
			wlListTableModel.addRow(newBook);
		}
		
		//Vector<BookObject>
		wlBookObjects = books;
	}
	
	//**********DELETE*TEST***********
	public String getPanel() {
		return "UserPrivateProfilePanel";
	}
	//**********DELETE*TEST***********


}
