package Client;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Stack;

import javax.swing.ImageIcon;
import javax.swing.JButton;

import CustomGUI.BlackButton;
import CustomGUI.BookitFrame;
import CustomGUI.BookitPanel;

public class MainFrame extends BookitFrame{
	private static final long serialVersionUID = 1L;

	private Client c;
	private NavBar navigation;
	private BookitPanel main_panel;
	private BookitPanel chat_p;
	public BlackButton chat;
	private boolean chat_check;
	private ChatDialog chatpanel;
	
	//store all panels that are visited; pop when going back
	public static Stack<BookitPanel> visitedPanels;
	
	/*
	 * Note: In order to update the center panel, update main_panel
	 */

	public MainFrame(Client c, ImageIcon profilePic) {
		super("BookIt");
		
		visitedPanels = new Stack<BookitPanel>();
		
		this.c = c;
		chat_check = true;
		if(c != null) {
			if(c.getUser() != null) {
				c.setMainFrame(this);
			}
		}
		chatpanel=null;
		instantiate(profilePic);
		createGUI();
		addAction();
		setVisible(true);
	}

	private void instantiate(ImageIcon profilePic) {
		navigation = new NavBar(this,c, profilePic);
		main_panel = new SearchPagePanel(this, c);
		
		chat = new BlackButton();
		chat.setText("Chat");
		//if visitor, disable chat
		if(c.getUser() == null) chat.setEnabled(false);
		
		chat_p = new BookitPanel();
		visitedPanels.add(main_panel);
	}

	private void createGUI(){
		//setLocation(Constants.frameWidth,Constants.frameHeight);
		this.getContentPane().setLayout(new BorderLayout());
		chat_p.setLayout(new BorderLayout());
		chat_p.add(chat,BorderLayout.SOUTH);
		this.getContentPane().add(navigation, BorderLayout.NORTH);
		this.getContentPane().add(main_panel,BorderLayout.CENTER);//
		this.getContentPane().add(chat_p,BorderLayout.SOUTH);
	}
	
	//return navbar
	public NavBar getNavBar() {
		return navigation;
	}

	private void addAction(){
				chat.addActionListener(new ActionListener() { // sign in attempt
						@Override
						public void actionPerformed(ActionEvent arg0) {
							if(chat_check == false){
								chat_check = true;
								chat_p.remove(chatpanel);
								chat_p.setPreferredSize(new Dimension(1024,35));
								chat_p.invalidate();
								chat_p.repaint();
								chat_p.revalidate();
								chat_p.repaint();
							}						
							else{
								if(c.getUser().getChatDialog()==null){
									chatpanel=new ChatDialog(c, null, "");
									chat_p.setPreferredSize(new Dimension(1024,450));
								}
								else{
									chatpanel=c.getUser().getChatDialog();
									chat_p.setPreferredSize(new Dimension(1024,450));
								}
								chat_p.add(chatpanel,BorderLayout.CENTER);
								chat_p.invalidate();
								chat_p.repaint();
								chat_p.revalidate();
								chat_p.repaint();
								chat_check=false;
							}
							
						}
					});
	}
	
	public ChatDialog getDialog(){
		return chatpanel;
	}
	
	//refresh mainframe with the most recently visited panel in stack
	public void refreshPanel() {
		BookitPanel p = visitedPanels.peek();
		this.getContentPane().remove(main_panel);
		this.getContentPane().add(p,BorderLayout.CENTER);
		main_panel = p;
		
		//**********DELETE*TEST***********
		System.out.println("Refresh panel in main frame: "+p.getPanel());
		//**********DELETE*TEST***********
		
		revalidate();
		repaint();
	}
	
	public void setcheck(){
		chat_check=true;
	}

	
}
