package Client;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;
import java.util.Vector;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

import CustomGUI.BookitPanel;
import Object.BookObject;
import Object.ChatObject;
import Object.MessageToClient;
import Object.MessageToServer;
import Object.User;
import Resource.Constants;

public class Client extends Thread implements Serializable {
	private static final long serialVersionUID = 1;
	 //
	private Socket s;
	private User user;
	private ObjectInputStream ois;
	private ObjectOutputStream oos;
	private MainFrame mf;
	private Set<String> chattingWith;
	
	// For signin or login failure pop-up
	private SigninGUI signinGUI;
	private SignupGUI signupGUI;
	
	private String email;
	private BookitPanel tempPanel;
	private BookObject book;
	private ImageIcon profile;
	private Lock lock;
	private Condition hasBook;
	
	//for loading
	private ImageIcon loadIcon = new ImageIcon("resource/loading.gif");
	private SplashScreen loadingScreen,loadingScreen_searchedUser,
						loadingScreen_getSearchedUserProfilePic;
						
	//public static SplashScreen loadingScreen_bookinfo;
	
		
	public Client(String hostname, int port) {
		// Display home page
		new HomePageGUI(this).setVisible(true);
		System.out.println("Displayed HomeGUI");
		user = null;
		mf = null;
		chattingWith = new HashSet<String>();
		tempPanel = null;
		book = null;
		profile = null;
		lock = new ReentrantLock();
		hasBook = lock.newCondition();

		try {
			s = new Socket(hostname, port);
			ois = new ObjectInputStream(s.getInputStream());
			oos = new ObjectOutputStream(s.getOutputStream());
		} catch (UnknownHostException e) {
			System.out.println("Client uhe: " + e.getMessage());
		} catch (IOException e) {
			System.out.println("Client ioe1: " + e.getMessage());
		}
		this.start();
		
		
		// TEST: chat using console (works)
		Scanner scan = null;
		scan = new Scanner(System.in);
		while(true) {
			String line = scan.nextLine();
			try {
				oos.writeObject(new ChatObject(getUser().getUsername(), "user2", line));
				oos.flush();
			} catch (IOException e) {
				System.out.println("Client type ioe: " + e.getMessage());
			} finally {
				scan.close();
			}
			
		}
	}
	
	public void run() {
		try {
			while(true) {
				// Listens to server & user's chat input (if chatting)
				MessageToClient rMsg = (MessageToClient)ois.readObject();
				String description = rMsg.getDescription();
				System.out.println("> Client received: " + description);
				
				if (description.equals(Constants.hasUsername)) { // Signup: if username is new, addUser & email & proceed to searchPageGUI
					if (!rMsg.getBool()) {
						MessageToServer emailCheckMsg = new MessageToServer(Constants.checkEmail, null, null, null, "", "", email,"",0,0);
						try {
							oos.writeObject(emailCheckMsg);
							oos.flush();
						} catch (IOException e) {
							System.out.println("Client ioe2: " + e.getMessage());
							// Display signin gui
							new SigninGUI(this);
//							signupGUI.proceedToSearchPage();

						}
					} else {
						System.out.println("Username exists already");
						user = null;
						JOptionPane.showMessageDialog(signupGUI, 
								"Username exists already",
								"Sign up failed",
								JOptionPane.WARNING_MESSAGE);
					}
				} else if (description.equals(Constants.checkEmail)) { 
					if (!rMsg.getBool()) {
						// Request profile pic
						
						// Tell server to add the user to DB
						MessageToServer signupMsg = new MessageToServer(Constants.addUser, null, user, null, "", "", email,"",0,0);
						try {
							oos.writeObject(signupMsg);
							oos.flush();
						} catch (IOException e) {
							System.out.println("Client ioe2: " + e.getMessage());
						}
						// Display signin gui
						new SigninGUI(this);
//						signupGUI.proceedToSearchPage();
					} else {
						System.out.println("Email exists already");
						user = null;
						JOptionPane.showMessageDialog(signupGUI, 
								"Email exists already",
								"Sign up failed",
								JOptionPane.WARNING_MESSAGE);
					}
					
				} else if (description.equals(Constants.signin)) { // Signin: if authenticated, proceed to searchPageGUI
					if (rMsg.getBool()) {
						// Get profile pic 
						if (rMsg.getUser() == null) {
							System.out.println("Client: signin is null");
						}
						System.out.println("Client: signin is good, getting profpic for " + rMsg.getUser().getUsername());
						getProfilePic(rMsg.getUser().getUsername());
//						signinGUI.proceedToSearchPage();
						
						//show loading
						loadingScreen = new SplashScreen(loadIcon);
						
					} else {
						user = null;
						JOptionPane.showMessageDialog(signupGUI, 
								"Invalid Username/Password",
								"Sign up failed",
								JOptionPane.WARNING_MESSAGE);
					}
				} else if (description.equals(Constants.getProfilePic)) {
					// Now that we have profile pic, proceed to search page (because we're signing in)
					System.out.println("Client: got profile pic");
					profile = rMsg.getProfilePic();
					
					//dispose loading
					loadingScreen.dispose();
					
					signinGUI.proceedToSearchPage(profile);
					
				} else if (description.equals(Constants.getProfilePicSearch)) {
					profile = rMsg.getProfilePic();
					((SearchPagePanel)tempPanel).UpdateSeachedUserProfilePic(profile);
					
					loadingScreen_getSearchedUserProfilePic.dispose();
					
				} else if (description.equals(Constants.forgotUsernamePw)) { // ForgotPwUsername: if email exists, success! 
					String successMsg = "Your username and temporary password has been sent to your email.";
					String failMsg = "Your email doesn't exist in our database.";
					String msg = "";
					if (rMsg.getBool()) msg = successMsg; 
					else msg = failMsg;
					JOptionPane.showMessageDialog(signupGUI, 
							msg,
							"Email sent",
							JOptionPane.INFORMATION_MESSAGE);
				} else if (description.equals(Constants.sendChat)) {
					// If it's a new chat, update numOngoingChats and add new chat-person to chattingWith
					MainFrame main_frame=getMainFrame();
					if (!chattingWith.contains(rMsg.getChat().getSenderName())) {
						
						chattingWith.add(rMsg.getChat().getSenderName());
						for(String s:chattingWith){
							System.out.println(s);
						}
						if(user.getChatDialog()==null){
							main_frame.chat.doClick();
							System.out.println("empty");
							main_frame.getDialog().add_tab(rMsg.getChat().getSenderName());
							main_frame.getDialog().append_text(rMsg.getChat().getMessage(),rMsg.getChat().getSenderName());
							getUser().setChatDialog(main_frame.getDialog());
						}
						else{
							System.out.println("noc exsit receive");
							main_frame.setcheck();
							main_frame.chat.doClick();
							main_frame.getDialog().add_tab(rMsg.getChat().getSenderName());
							main_frame.getDialog().append_text(rMsg.getChat().getMessage(),rMsg.getChat().getSenderName());
							main_frame.getDialog().set_select(rMsg.getChat().getSenderName());
						}
					}
					else{
						System.out.println("exsit");
						main_frame.setcheck();;
						main_frame.chat.doClick();
						main_frame.getDialog().set_select(rMsg.getChat().getSenderName());
						main_frame.getDialog().append_text(rMsg.getChat().getMessage(),rMsg.getChat().getSenderName());
						
					}
				} else if (description.equals(Constants.getUser)) {
					((SearchPagePanel)tempPanel).getUser(rMsg.getUser());
					
					loadingScreen_searchedUser.dispose();
					
				} else if (description.equals(Constants.checkOnline)) {
					Boolean isOnline = rMsg.getBool();
					if(tempPanel instanceof UserPublicProfilePanel){
						((UserPublicProfilePanel) tempPanel).updateSearchedUserIsOnline(isOnline);
						
					}
					
					// TODO: privPanel
					
				} else if(description.equals(Constants.Onlinechat)){
					Boolean isOnline = rMsg.getBool();
					MainFrame main_frame=getMainFrame();
					main_frame.setcheck();
					main_frame.chat.doClick();
					if(isOnline){
						main_frame.getDialog().sendtext();
					}else{
					    main_frame.getDialog().disable_button(rMsg.getUser().getUsername());
					}			

					
				} else if(description.equals(Constants.updateEmail)) {
					Boolean existsAlready = rMsg.getBool();
					if (existsAlready) {
						JOptionPane.showMessageDialog((EditProfilePanel)tempPanel, 
								"Error",
								"Email exists already.",
								JOptionPane.INFORMATION_MESSAGE);
					}
				} else if (description.equals(Constants.uploadProfilePic)) {
					// TODO: call updateImage in editprofilepanel
					((EditProfilePanel)tempPanel).updateimage(profile);
				}
				
				
				
				else if (description.equals(Constants.getBooks)) {
					Vector<BookObject> booksResult = rMsg.getBooks();
					((SearchPagePanel)tempPanel).deliverBooks(booksResult);
					
					// loadingScreen_searchbook.dispose();
					
				} /*else if (description.equals(Constants.getBook)) {
					((UserPrivateProfilePanel)tempPanel).showBook(rMsg.getBook());
				}*/ else if (description.equals(Constants.getSellBooksPriv)) {
					Vector<BookObject> sellBooks = rMsg.getBooks();
					if (sellBooks == null) {
						System.out.println("PrivProf sellbook = null");
					}
						((UserPrivateProfilePanel)tempPanel).populateSellBookList(sellBooks);
				} else if (description.equals(Constants.getSellBooksPub)) {
					Vector<BookObject> sellBooks = rMsg.getBooks();
					System.out.println("Client: populate PrivateProf");
					((UserPublicProfilePanel)tempPanel).populateSellBookList(sellBooks);
				} else if (description.equals(Constants.getWLBooks)) {
					Vector<BookObject> book = rMsg.getBooks();
					((UserPrivateProfilePanel)tempPanel).populateWL(book);
				} else if (description.equals(Constants.addBook)) {
					Boolean existsAlready = rMsg.getBool();
					if (!existsAlready) {
						Vector<BookObject> sellBooks = rMsg.getBooks();
						((UserPrivateProfilePanel)tempPanel).populateSellBookList(sellBooks);
						JOptionPane.showMessageDialog((UserPrivateProfilePanel)tempPanel, "New book added successfully!",
								"Success", JOptionPane.INFORMATION_MESSAGE);
					} else {
						JOptionPane.showMessageDialog((UserPrivateProfilePanel)tempPanel, "Book you wanted to add exists already.",
								"Error", JOptionPane.ERROR_MESSAGE);
					}
				} else if (description.equals(Constants.addToWL)) {
					Vector<BookObject> books = rMsg.getBooks();
					if (book == null) {
						System.out.println("Client: WL is empty");
					} else {
						for (BookObject b : books) {
							System.out.println("Client WL added: " + b.getTitle() + "-" + b.getSellerUsername());
						}
					}
					// If it was a privateProfile (like not SearchPage) that called addToWL, then update it
//					if (tempPanel instanceof UserPrivateProfilePanel) {
//						((UserPrivateProfilePanel)tempPanel).populateWL(books);
//					}
				} else if (description.equals(Constants.deleteBook)) {
					Vector<BookObject> book = rMsg.getBooks();
					((UserPrivateProfilePanel)tempPanel).populateSellBookList(book);
				} else if (description.equals(Constants.deleteBookWL)) {
					Vector<BookObject> book = rMsg.getBooks();
					((UserPrivateProfilePanel)tempPanel).populateWL(book);
				} else if (description.equals(Constants.getImages)) {
					lock.lock();
					System.out.println("client got the images");
					
			    	//Client.loadingScreen_bookinfo.dispose();
					
					book = rMsg.getBook();
					hasBook.signalAll();					
					lock.unlock();
									
				} else if (description.equals(Constants.getSellerAndProfilePic)) {
					profile = rMsg.getProfilePic();
					((BookInfoPanel)tempPanel).contactSeller(profile, rMsg.getUser());;
				}
			}
		} catch (ClassNotFoundException e) {
			System.out.println("Client cnfe: " + e.getMessage());
		} catch (IOException e) {
			System.out.println("Client ioe3: " + e.getMessage());
			e.printStackTrace();//***DELETE***
		} finally {
//			System.out.println("Inside Client finally");
//			if (s != null) {
//				try {
//					s.close();
//				} catch (IOException e) {
//					System.out.println("Client ioe4: " + e.getMessage());
//				}
//			}
			//If server can't be reached... 
		}
	}
	
	public void createUser(String username, String pw, String email, SignupGUI signupGUI) {
		this.signupGUI = signupGUI;
		user = new User(username, pw); 
		this.email = email;
		
		MessageToServer checkUsernameMsg = new MessageToServer(Constants.hasUsername, null, null, null, username, "", "","",0,0);
		try {
			oos.writeObject(checkUsernameMsg);
			oos.flush();
			System.out.println("Client: sent ...");
			System.out.println("Creating user " + username);
		} catch (IOException e) {
			System.out.println("Client ioe5: " + e.getMessage());
		}
	}
	
	public void authenticateUser(String username, String pw, SigninGUI signinGUI) {
		
		System.out.println("  Authenticating user " + username);
		this.signinGUI = signinGUI;
		user = new User(username, pw);

		MessageToServer msg = new MessageToServer(Constants.signin, null, null, null, username, pw, "","",0,0);
		try {
			oos.writeObject(msg);
			oos.flush();
		} catch (IOException e) {
			System.out.println("Client ioe6: " + e.getMessage());
		}
	}
	
	// To chat with another user 
	public void sendMessage(String receiverUsername, String line) {
		ChatObject chat = new ChatObject(getUser().getUsername(), receiverUsername, line);
		MessageToServer msg = new MessageToServer(Constants.sendChat,null,null,chat,"","","","",0,0);
		System.out.println("Client: sendMessage to "+receiverUsername+" " + line);
		try {
			oos.writeObject(msg);
			oos.flush();
		} catch (IOException e) {
			System.out.println("Client sendMessage: " + e.getMessage());
		}
	}
	
	// Checks if user I want to chat with is online
	public void checkOnline(String username,UserPublicProfilePanel userpublic) {
		tempPanel = userpublic;
		MessageToServer msg = new MessageToServer(Constants.checkOnline,null,null,null,username,"","","",0,0);
		try {
			oos.writeObject(msg);
			oos.flush();
		} catch (IOException e) {
			System.out.println("Client checkOnline: " + e.getMessage());
		}
	}
	
	public void checkonline_chat(String username){
		MessageToServer msg = new MessageToServer(Constants.Onlinechat,null,null,null,username,"","","",0,0);
		try {
			oos.writeObject(msg);
			oos.flush();
		} catch (IOException e) {
			System.out.println("Client checkOnline_chat: " + e.getMessage());
		}
	}
	
	// Add to sell list
	public void addBook(BookObject newBook) {
		MessageToServer msg = new MessageToServer(Constants.addBook, newBook, null, null, "", "", "","",0,0);
		System.out.println("Client addBook: " + newBook.getTitle());
		try {
			oos.writeObject(msg);
			oos.flush();
		} catch (IOException e) {
			System.out.println("Client addBook: " + e.getMessage());
		}
	}
	
	// Add to WL
	public void addBookWL(BookObject newBook/*, BookitPanel p*/) {
//		tempPanel = p;		
		MessageToServer msg = new MessageToServer(Constants.addToWL, newBook, null, null, getUser().getUsername(), "", "","",0,0);
		System.out.println("Client addToWL: " + newBook.getSellerUsername() + "'s " + newBook.getTitle());
		try {
			oos.writeObject(msg);
			oos.flush();
		} catch (IOException e) {
			System.out.println("Client addWL: " + e.getMessage());
		}
	}
	
	// Delete from sell list
//	public void deleteBook(BookObject toDelete, UserPrivateProfilePanel priv) {
	public void deleteBook(String title, String seller, UserPrivateProfilePanel priv) {
		tempPanel = priv;
		BookObject toDelete = new BookObject(title,"",0,0,"","", 0, seller);
		MessageToServer msg = new MessageToServer(Constants.deleteBook, toDelete, null, null, seller, "", "","",0,0);
		System.out.println("Client delete: " + toDelete.getTitle());
		try {
			oos.writeObject(msg);
			oos.flush();
		} catch (IOException e) {
			System.out.println("Client delete: " + e.getMessage());
		}
	}
	
	// Delete from WL
	public void deleteBookWL(String title, String seller, UserPrivateProfilePanel priv) {
		tempPanel = priv;
		BookObject toDelete = new BookObject(title,"",0,0,"","", 0, seller);
		MessageToServer msg = new MessageToServer(Constants.deleteBookWL, toDelete, null, null, getUser().getUsername(), "", "","",0,0);
		System.out.println("Client deleteWL: " + toDelete.getTitle() + " sold by " + toDelete.getSellerUsername());
		try {
			oos.writeObject(msg);
			oos.flush();
		} catch (IOException e) {
			System.out.println("Client deleteWL: " + e.getMessage());
		}
	}
	
	// Request books user is selling
	public void getSellBooksPriv(String username, UserPrivateProfilePanel priv) {
		tempPanel = priv;
		try {
			MessageToServer msg = new MessageToServer(Constants.getSellBooksPriv, null, null, null, username, "", "","",0,0);
			oos.writeObject(msg);
			oos.flush();
		} catch (IOException e) {
			System.out.println("Client ioe10: " + e.getMessage());
		}
	}
	public void getSellBooksPub(String username, UserPublicProfilePanel pub) {
		tempPanel = pub;
		try {
			MessageToServer msg = new MessageToServer(Constants.getSellBooksPub, null, null, null, username, "", "","",0,0);
			oos.writeObject(msg);
			oos.flush();
		} catch (IOException e) {
			System.out.println("Client ioe10: " + e.getMessage());
		}
	}
	
	// Request books on user's wish list fdg
	public void getWLBooks(String username, UserPrivateProfilePanel priv) {
		tempPanel = priv;
		try {
			MessageToServer msg = new MessageToServer(Constants.getWLBooks, null, null, null, username, "", "","",0,0);
			oos.writeObject(msg);
			oos.flush();
		} catch (IOException e) {
			System.out.println("Client ioe11: " + e.getMessage());
		}
	}
	
	public void sendOfflineMessage(String toUser, String message, String fromUser) {
		MessageToServer msg = new MessageToServer(Constants.offlineMessage, null, new User(fromUser, ""), null, toUser, "", "",message,0,0);
		try {
			oos.writeObject(msg);
			oos.flush();
		} catch (IOException e) {
			System.out.println("Client offlineMsg: " + e.getMessage());
		}
	}
	
	public void forgotUsernamePw(String email, SigninGUI signinGUI) {
		this.signinGUI = signinGUI;
		try {
			MessageToServer msg = new MessageToServer(Constants.forgotUsernamePw, null, null, null, "", "", email,"",0,0);
			oos.writeObject(msg);
			oos.flush();
		} catch (IOException e) {
			System.out.println("Client ioe12: " + e.getMessage());
		}
	}
	
	public void updatePw(String username, String pw) {
		MessageToServer msg = new MessageToServer(Constants.updatePw, null,null,null,username,pw,"","",0,0);
		try {
			oos.writeObject(msg);
			oos.flush();
		} catch (IOException e) {
			System.out.println("Client updatePw: " + e.getMessage());
		}
	}
	
	public void updateEmail(String username, String email, EditProfilePanel panel) {
		tempPanel = panel;
		this.email = email;
		MessageToServer msg = new MessageToServer(Constants.updateEmail, null,null,null,username,"",email,"",0,0);
		try {
			oos.writeObject(msg);
			oos.flush();
		} catch (IOException e) {
			System.out.println("Client updateEmail: " + e.getMessage());
		}
	}
	
	public User getUser() {
		return user;
	}
	
	public MainFrame getMainFrame() {
		return mf;
	}
	
	public void logout() {
		MessageToServer msg = new MessageToServer(Constants.logout, null,null,null,user.getUsername(),"","","",0,0);
		try {
			oos.writeObject(msg);
			oos.flush();
		} catch (IOException e) {
			System.out.println("Client logout: " + e.getMessage());
		}
		user = null;
		System.out.println("Client: log-out");
//		if (s != null) {
//			try {
//				s.close();
//			} catch (IOException e) {
//				System.out.println("Client logout: " + e.getMessage());
//			}
//		}
	}
	
	// For searching books
	public void getBookSearch(String searchWords, int criteria, int sortCriteria, SearchPagePanel searchPagePanel) {
		tempPanel = searchPagePanel;
		MessageToServer msg = new MessageToServer(Constants.getBooks, null, null, null, "", "", "",searchWords,criteria, sortCriteria);
		
		// loadingScreen_searchbook = new SplashScreen(loadIcon);
		
		try {
			oos.writeObject(msg);
			oos.flush();
		} catch (IOException e) {
			System.out.println("Client ioe13: " + e.getMessage());
		}
	}
	
	// For searching users
	public void getUserSearch(String usernameSearch, SearchPagePanel searchPagePanel) {
		tempPanel = searchPagePanel;
		MessageToServer msg = new MessageToServer(Constants.getUser, null, null, null, "", "", "",usernameSearch,0,0);
		
		loadingScreen_searchedUser = new SplashScreen(loadIcon);
		
		try {
			oos.writeObject(msg);
			oos.flush();
		} catch (IOException e) {
			System.out.println("Client ioe14: " + e.getMessage());
		}
	}
	
	// When user we're chatting with is offline, update numOngoingChats & chattingWith
	public void removeChatWith(String username) {
		chattingWith.remove(username);
		user.getChatDialog().remove_tab(username);
	}
	
	public void add_chat(String name){
		chattingWith.add(name);
	}

	public void getImages(BookObject book) {
		MessageToServer msg = new MessageToServer(Constants.getImages, book, null, null, "", "", "","",0,0);
		
		//loadingScreen_bookinfo = new SplashScreen(loadIcon);
		
		try {
			oos.writeObject(msg);
			oos.flush();
		} catch (IOException e) {
			System.out.println("Client ioe15: " + e.getMessage());
		}
	}
	
	public void getProfilePic(String username) {
		MessageToServer msg = new MessageToServer(Constants.getProfilePic, null, null, null, username, "", "","",0,0);
		try {
			oos.writeObject(msg);
			oos.flush();
			System.out.println("client getting profile pic");
		} catch (IOException e) {
			System.out.println("Client getProfile: " + e.getMessage());
		}
	}
	
	public void getSearchedUserProfilePic(String toSearchUser, SearchPagePanel searchPagePanel) {
		tempPanel = searchPagePanel;
		MessageToServer msg = new MessageToServer(Constants.getProfilePicSearch, null, null, null, toSearchUser, "", "","",0,0);
		
		loadingScreen_getSearchedUserProfilePic = new SplashScreen(loadIcon);
		
		try {
			oos.writeObject(msg);
			oos.flush();
			System.out.println("client getting profile pic for " + toSearchUser);
		} catch (IOException e) {
			System.out.println("Client searchProfile: " + e.getMessage());
		}
	}
	
	public void getSellerAndProfilePic(String toSearchUser, BookInfoPanel bookinfoPanel) {
		tempPanel = bookinfoPanel;
		MessageToServer msg = new MessageToServer(Constants.getSellerAndProfilePic, null, null, null, toSearchUser, "", "","",0,0);
		try {
			oos.writeObject(msg);
			oos.flush();
			System.out.println("client getting profile pic for " + toSearchUser);
		} catch (IOException e) {
			System.out.println("Client searchProfile: " + e.getMessage());
		}
	}
	
	public void setMainFrame(MainFrame mf) {
		this.mf = mf;
	}
	
	public ImageIcon getProfilePic() {
		return profile;
	}
	
	public void setBookToNull() {
		book = null;
	}
	
	public BookObject getBook() {
		lock.lock();
		if (book == null) {
			try {
				hasBook.await();
			} catch (InterruptedException ie) {
				System.out.println("ie in getBook: " + ie.getMessage());
			}
		}
		lock.unlock();		
		return book;
	}
	
	/*public void getBook(String title, String sellerName, UserPrivateProfilePanel priv) { // returns a book for profile
		tempPanel = priv;
		BookObject emptyBook = new BookObject(title,"",0,0,"","",0, sellerName);
		MessageToServer msg = new MessageToServer(Constants.getBook, emptyBook, null, null, "", "", "","",0,0);
		try {
			oos.writeObject(msg);
			oos.flush();
		} catch (IOException e) {
			System.out.println("Client getBook(one): " + e.getMessage());
		}
	}*/

	public void setBook(BookObject book) {
		this.book = book;
	}
	
	public void removechat(String name){
		chattingWith.remove(name);
	}
	
	public void uploadImage(ImageIcon icon, String username, EditProfilePanel editPanel) {
		profile = icon;
		tempPanel = editPanel;
		MessageToServer msg = new MessageToServer(Constants.uploadProfilePic, null, null, null, username, "", "","",0,0);
		msg.setIcon(icon);
		try {
			oos.writeObject(msg);
			oos.flush();
		} catch (IOException e) {
			System.out.println("Client ioe17: " + e.getMessage());
		}
	}


	public static void main(String[] args) {
		new Client(Constants.hostname, Constants.port);
	}

}
