package Client;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import CustomGUI.BookitFrame;
import CustomGUI.TransparentButton;
import CustomGUI.TransparentPanel;
import Resource.ImageLibrary;

public class HomePageGUI extends BookitFrame {
	
	private JPanel imagePanel;
	private JButton signupButton, signinButton, visitorButton;
	
	private JLabel nameLabel;
	
	private Client mClient;
	
	public HomePageGUI(Client inClient) {
		super("BookIt");
		
		mClient = inClient;
		
		initializeComponents();
		createGUI();
		addAction();
	}
	
	private void initializeComponents() {
		nameLabel = new JLabel("BookIt");
		nameLabel.setOpaque(false);
		
		
		imagePanel = new TransparentPanel();
		imagePanel.setLayout(new GridBagLayout());
		//imagePanel.setSize(new Dimension(this.getWidth()/2,this.getHeight()/2));;
		
		signupButton = new TransparentButton();
		Image signupImage = ImageLibrary.getImage("resource/img/homepage/signup.png");
		signupButton.setIcon(new ImageIcon(signupImage));
		signupButton.setToolTipText("Sign up");
		
		signinButton = new TransparentButton();
		Image signinImage = ImageLibrary.getImage("resource/img/homepage/signin.png");
		signinButton.setIcon(new ImageIcon(signinImage));
		signinButton.setToolTipText("Sign in");
		
		visitorButton = new TransparentButton();
		Image visitorImage = ImageLibrary.getImage("resource/img/homepage/visitor.png");
		visitorButton.setIcon(new ImageIcon(visitorImage));
		visitorButton.setToolTipText("Visitor");
	}
	
	private void createGUI() {
		getBookitContentPane().setLayout(new GridBagLayout());
		GridBagConstraints mainGbc = new GridBagConstraints();

		GridBagConstraints ipGbc = new GridBagConstraints();
		ipGbc.gridx = 0;
		ipGbc.gridy = 0;
		imagePanel.add(signupButton, ipGbc);
		
		ipGbc.gridx = 1;
		ipGbc.gridy = 0;
		imagePanel.add(signinButton, ipGbc);
		
		ipGbc.gridx = 2;
		ipGbc.gridy = 0;
		imagePanel.add(visitorButton, ipGbc);
		
		//mainGbc.fill = GridBagConstraints.HORIZONTAL;
		mainGbc.gridx = 0;
		mainGbc.gridy = 0;
		
		getBookitContentPane().add(nameLabel, mainGbc);
		
		//mainGbc.fill = GridBagConstraints.HORIZONTAL;
		mainGbc.gridx = 0;
		mainGbc.gridy = 2;
		mainGbc.ipady = 100;
		getBookitContentPane().add(imagePanel, mainGbc);
	}
	
	private void addAction() {
		signupButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new SignupGUI(mClient);
				HomePageGUI.this.dispose();
			}
		});
		
		signinButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new SigninGUI(mClient);
				HomePageGUI.this.dispose();
			}
		});
		
		visitorButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ImageIcon icon = new ImageIcon(ImageLibrary.getImage("resource/img/anon.jpg"));
				new MainFrame(mClient, icon);
				HomePageGUI.this.dispose();
			}
		});
		
	}
	
	public static void main(String[] args) {
		HomePageGUI home = new HomePageGUI(null);
		home.setVisible(true);
	}
}
