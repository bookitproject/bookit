package Client;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.Serializable;
import java.util.Vector;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;

import javax.swing.JTextField;

import CustomGUI.BlackButton;
import CustomGUI.BookitScrollPane;
import CustomGUI.EmojiTextPane;
import CustomGUI.RoundTextfiled;
import CustomGUI.TransparentButton;
import CustomGUI.TransparentPanel;
import Resource.ImageLibrary;

public class ChatDialog extends TransparentPanel implements Serializable {
	private static final long serialVersionUID = 1;
	
	private JTabbedPane tabbedPane;
	private Client client;
	private Vector<TransparentPanel> total_panel;
	private Vector<EmojiTextPane> total_area;
	private Vector<RoundTextfiled> total_field;
	private Vector<TransparentButton> total_button;
	private Vector<String> total_name;
	private Vector<TransparentButton> total_close;
	public int number;
	private EmojiTextPane text1;
	private String input_text1;
	private String username_real;
	private int number_count;

	
	public ChatDialog(Client client,String username ,String receiver){
		super();
		number=0;
		this.client=client;
		instantiate(); 
		createGUI();
		number_count=0;
		setVisible(true);
	}
	
	private void instantiate() {
		tabbedPane=new JTabbedPane();
		total_panel=new Vector<TransparentPanel>();
		total_area=new Vector<EmojiTextPane>();
		total_field=new Vector<RoundTextfiled>();
		total_button=new Vector<TransparentButton>();
		total_name=new Vector<String>();
		total_close=new Vector<TransparentButton>();
		
	}
	private void createGUI(){
		
		this.add(tabbedPane);
		tabbedPane.setPreferredSize(new Dimension(980,520));
		this.setPreferredSize(new Dimension(1024,540));
	}
	
	
	public void add_tab(String username1){
		number++;
		total_name.add(username1);
		TransparentPanel first_panel_new=new TransparentPanel();
		first_panel_new.setLayout(new BoxLayout(first_panel_new,BoxLayout.Y_AXIS));
		EmojiTextPane first_area_new=new EmojiTextPane();
		//first_area_new.setLineWrap(true);
		RoundTextfiled first_input_new=new RoundTextfiled();
		
		Image one = ImageLibrary.getImage("resource/emoji/1.png"); //***TESTING*DELETE***
		one = one.getScaledInstance(50, 50, java.awt.Image.SCALE_SMOOTH) ;
		Image two = ImageLibrary.getImage("resource/emoji/2.png"); //***TESTING*DELETE***
		two = two.getScaledInstance(50, 50, java.awt.Image.SCALE_SMOOTH) ; 
		Image thr = ImageLibrary.getImage("resource/emoji/3.png"); //***TESTING*DELETE***
		thr = thr.getScaledInstance(50, 50, java.awt.Image.SCALE_SMOOTH) ; 
		Image four = ImageLibrary.getImage("resource/emoji/4.png"); //***TESTING*DELETE***
		four = four.getScaledInstance(50, 50, java.awt.Image.SCALE_SMOOTH) ; 
		Object[] items =
	        {	
	        	new ImageIcon(one),
	        	new ImageIcon(two),
	        	new ImageIcon(thr),
	        	new ImageIcon(four),
	        };
	    JComboBox<Object> comboBox= new JComboBox<Object>( items );
	    comboBox.setPreferredSize(new Dimension(50,50));

		BlackButton chatbutton_new=new BlackButton();
		chatbutton_new.setText("CHAT");
		total_panel.add(first_panel_new);
		total_area.add(first_area_new);
		total_field.add(first_input_new);
		total_button.add(chatbutton_new);
		BookitScrollPane scroll_new = new BookitScrollPane (first_area_new);
		scroll_new.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);	
		scroll_new.setPreferredSize(new Dimension(800,200));
		TransparentPanel lower_new= new TransparentPanel();
		lower_new.setLayout(new FlowLayout());
		first_input_new.setPreferredSize(new Dimension(600,40));
		BlackButton close_button=new BlackButton();
		close_button.setText("CLOSE");
		total_close.add(close_button);
		lower_new.add(comboBox);
		lower_new.add(first_input_new);
		lower_new.add(chatbutton_new);
		lower_new.add(close_button);
		first_panel_new.add(scroll_new/*,BorderLayout.CENTER*/);
		first_panel_new.add(lower_new/*,BorderLayout.SOUTH*/);
		tabbedPane.add(username1, first_panel_new);
		chatbutton_new.addActionListener(new ActionListener(){	
			public void actionPerformed(ActionEvent ae){	
				int index=tabbedPane.getSelectedIndex();
				EmojiTextPane text=total_area.get(index);
				JTextField input=total_field.get(index);
				String input_text=input.getText();
				input.setText("");
				client.checkonline_chat(username1);
				text1=text;
				username_real=username1;
				input_text1=input_text;			
				
			}
		});
		first_input_new.addKeyListener(new KeyAdapter(){
	         public void keyPressed(KeyEvent e) {
	           int key = e.getKeyCode();
	           if (key == KeyEvent.VK_ENTER) {
	        	   chatbutton_new.doClick();
	           }
	         }
		});
		
		close_button.addActionListener(new ActionListener(){	
			public void actionPerformed(ActionEvent ae){
	        	 System.out.println("fvgrbgdf");
	        	 remove_tab(username1);
	        	 client.removechat(username1);
	           }
			});

		comboBox.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent arg0) {
				if(arg0.getStateChange() == ItemEvent.SELECTED) {
					int emoji_index = comboBox.getSelectedIndex();
					int input_index=tabbedPane.getSelectedIndex();
					JTextField input=total_field.get(input_index);
				    input.setText(input.getText() + "/"+(emoji_index+1)+" ");
                }
			}
		});

	}
	
	public void sendtext(){
		text1.append("Me:"+input_text1+"\n");
		client.sendMessage(username_real,input_text1);
	}
	
	public void disable_button(String name){
		int index=total_name.indexOf(name);
		System.out.println(number);
		if(number>0){
			for(ActionListener act : total_button.get(index).getActionListeners()) {
				total_button.get(index).removeActionListener(act);  
			}
			
			total_button.get(index).addActionListener(new ActionListener(){
				
				public void actionPerformed(ActionEvent ae){	
					
					JOptionPane.showMessageDialog(ChatDialog.this,
							"OFFLINE,THE TAB WILL BE CLOSED",
							"OFFLINE WARNING",
							JOptionPane.ERROR_MESSAGE);
					remove_tab(name);   
				}
			});
		}
		
		number_count++;
		if(number_count==1){
			System.out.println(number);
			total_button.get(index).doClick();
		}
		
	}
	
	public void remove_tab(String username2){
		number--;
		int index=0;
		for(String name:total_name){
			if(name.equals(username2)){
			    break;
			}
			index++;
		}
		total_panel.remove(index);
		total_area.remove(index);
		total_field.remove(index);
		total_button.remove(index);
		total_name.remove(index);
		total_close.remove(index);
		tabbedPane.remove(index);
	}
	
	public void append_text(String text,String name){
		int index=0;
		for(String user_name:total_name){
			if(user_name.equals(name)){
			    break;
			}
			index++;
		}
		EmojiTextPane area=total_area.get(index);
		area.append(name+": "+text+"\n");
		
	}
	
	public boolean exsit(String name){
		if(total_name.contains(name)){
			return true;
		}
		return false;
	}
	
	
	public void set_select(String name){
		int index=0;
		for(String user_name:total_name){
			if(user_name.equals(name)){
			    break;
			}
			index++;
		}
		tabbedPane.setSelectedIndex(index);
	}
	
	/*public static void main(String[] args){
		new ChatDialog(null,"Sfvg",null);
	}*/

}
