package Client;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.geom.Ellipse2D;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;

public class SplashScreen {
	private JFrame frame;
	
	public SplashScreen(ImageIcon loadIcon) {
    	EventQueue.invokeLater(new Runnable(){
    		  
    		  public void run(){
    		    JLabel label = new JLabel();

    		    label.setHorizontalAlignment(SwingConstants.CENTER);
	    
    		    label.setIcon(loadIcon);

    		    frame = new JFrame();
    		    frame.setSize(new Dimension(200, 200));
    		    frame.setUndecorated(true);
    		    frame.setOpacity(0.8f);
    		    frame.setLocationRelativeTo(null);
    		    frame.setBackground(Color.BLACK); 
    		    frame.setShape(new Ellipse2D.Double(0,0,200,200));
    		    frame.add(label, BorderLayout.CENTER);
    		    frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    		    
    		    frame.setVisible(true);
    		  }
    	});
    }
	
	public void show() {
		frame.setVisible(true);
	}
	
	public void unshown() {
		frame.setVisible(false);
	}
	
	public void dispose() {
		//if(frame != null) 
		frame.dispose();
	}
	
	/*public static void main(String[] args) { 
		ImageIcon loadIcon = new ImageIcon("resource/loading.gif");
		new SplashScreen(loadIcon);
	}*/

}
