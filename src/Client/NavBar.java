package Client;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import CustomGUI.TransparentButton;
import CustomGUI.TransparentPanel;
import CustomGUI.WhiteButton;
import Resource.ImageEditor;
import Resource.ImageLibrary;

public class NavBar extends JPanel {

	private static final long serialVersionUID = 1;
	private Client mClient;
	private MainFrame mMainFrame;
	
	//private TransparentPanel leftPanel
	private JPanel profilePicPanel;
	private JLabel nameLabel;
	private TransparentButton homeButton;
	private WhiteButton profileButton, logoutButton, signupButton, signinButton;
	private ImageIcon profilepic;
	
	public NavBar(MainFrame inMainFrame, Client inClient, ImageIcon inProfilePic) {
		super();
		mClient = inClient;
		mMainFrame = inMainFrame;
		profilepic = inProfilePic;
		
		if(mClient != null) {
			//mClient.getProfilePic(mClient.getUser().getUsername());
			initializeComponents();
			createGUI();
			addAction();
		}
	}
	
	public void initializeComponents() {
		homeButton = new TransparentButton();
		homeButton.setPreferredSize(new Dimension(50,50));
		Image image = ImageLibrary.getImage("resource/img/home.png");	
		image = image.getScaledInstance(50, 50, Image.SCALE_SMOOTH) ; 
		homeButton.setIcon(new ImageIcon(image));
		
		if(mClient.getUser() != null) {
			nameLabel = new JLabel(mClient.getUser().getUsername());
			profileButton = new WhiteButton();
			profileButton.setText("profile");
			logoutButton = new WhiteButton();
			logoutButton.setText("log out");
		}
		else {
			nameLabel = new JLabel("visitor");
			signupButton = new WhiteButton();
			signupButton.setText("sign up");
			signinButton = new WhiteButton();
			signinButton.setText("sign in");
		}
		
		nameLabel.setFont(nameLabel.getFont().deriveFont(32.0f));
		
		System.out.println(this.getHeight());
		profilePicPanel = new TransparentPanel();
		/*mClient.getProfilePic(mClient.getUser().getUsername());
//		if (profilepic == null) {
//			boolean gotProfilePic = false;
//			while (!gotProfilePic) {
//				gotProfilePic = mClient.getGotProfilePic();
//				profilepic = mClient.getProfilePic();
//			}
//			mClient.setGotProfilePicToFalse();
//		}
//		profilepic = mClient.getProfilePic();
		Image profilePic = ImageLibrary.getImage("resource/img/sample_profilePic.jpg");*/
		Image profilePic = profilepic.getImage();
		BufferedImage buff = ImageLibrary.getRenderedImage(profilePic);
		BufferedImage masked = ImageEditor.makeRoundedCorner(buff, 100);
		profilePicPanel.add(new JLabel(new ImageIcon(masked)));
		
	}
	
	public void createGUI() {
		this.setPreferredSize(new Dimension(100,100));
		setBackground(new Color(0,0,0,250));
		setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();	
		gbc.gridx = 0;
		gbc.gridy = 0;
		add(profilePicPanel, gbc);
		gbc.gridx = 1;
		gbc.gridy = 0;
		add(nameLabel, gbc);
		
		gbc.insets = new Insets(0,300,0,0);
		gbc.gridx = 2;
		gbc.gridy = 0;
		add(homeButton, gbc);	
		
		gbc.insets = new Insets(0,200,0,0);
		if(mClient.getUser() != null) {
			gbc.gridx = 3;
			gbc.gridy = 0;
			add(profileButton, gbc);
			
			gbc.insets = new Insets(10,10,10,10);
			gbc.gridx = 4;
			gbc.gridy = 0;
			add(logoutButton, gbc);
		}
		else {
			gbc.gridx = 3;
			gbc.gridy = 0;
			add(signupButton, gbc);
			gbc.insets = new Insets(0,0,0,0);
			gbc.gridx = 4;
			gbc.gridy = 0;
			add(signinButton, gbc);
		}
	}
	
	public void addAction() {
		homeButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MainFrame.visitedPanels.clear();
				SearchPagePanel new_panel = new SearchPagePanel(mMainFrame, mClient);
				MainFrame.visitedPanels.add(new_panel);
				
				mMainFrame.refreshPanel();
			}
		});
		
		if(mClient.getUser() != null) {
			profileButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {				
					UserPrivateProfilePanel new_panel = new UserPrivateProfilePanel(mClient, mMainFrame);
					MainFrame.visitedPanels.add(new_panel);
					
					mMainFrame.refreshPanel();
				}
			});
			
			logoutButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					new HomePageGUI(mClient).setVisible(true);
					mMainFrame.dispose();
					mClient.logout();
					System.out.println("NavBar: logging out");
				}
			});
		}
		else {
			signupButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {					
					new SignupGUI(mClient);
					mMainFrame.dispose();
				}
			});
			
			signinButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					new SigninGUI(mClient);
					mMainFrame.dispose();
				}
			
			});
		}
	}
	
	public void setProfileImage(ImageIcon icon) {
		profilepic = icon;
		profilePicPanel.remove(0);
		profilePicPanel.add(new JLabel(icon));
	}
	
	public Image returnProfilepic(){
		return profilepic.getImage();
	}
	/*public static void main(String[] args) {
		BookitFrame bif = new BookitFrame("NAVBAR TESTING");
		bif.getContentPane().add(new NavBar(null), BorderLayout.NORTH);
		bif.setVisible(true);
	}*/
}
