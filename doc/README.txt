Group Members
Jessie Guo yangzigu@usc.edu 30393
Yining Huang yininghu@usc.edu 30381
Emma Wang yingchew@usc.edu 30381
Ivy Lin ivylin@usc.edu 29928
Angela Shao annchies@usc.edu 30393

What works:
- Book search returns books with titles containing search words
- User serach returns user with the given username
- Added books appears in search
- Deleted books are removed from wish lists
- Profiles are editable (including profile picture)
- Password and emails can be changed
- Visitors are only able to search and cannot chat/contact a user
- Back buttons correctly brings user back to the last "page" (i.e. panel) viewed
- 

Note:
- "Contact Seller" button in public profiles opens a new tab in the chat dialog while "Chat" button at the bottom of the frame simply shows/hides the chat dialog
- "Close" button in the chat dialog deletes chat messages for the current chat from the user's view